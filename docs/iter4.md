Iteration 4
===========

Timeline
--------

Iteration 4 is the period **Starting** at the end of the [Iteration 3 meeting](iter3.md) and **Ending** at the beginning of Iteration 4 demo time 
during **12/13 - 12/14 (Thu - Fri)**. _This is the last iteration, and your code and document must be completed no later than 12/14._


Final Code Submission
---------------------

- __Due 12/14 11:59pm__, please tag the final version (code, tests, and documentation) as ``Final``.
- Your tagged code shall exhibit the following:
    1. __Short comments__ have been provided for all public methods and classes 
       following the proper commenting conventions (i.e., Javadoc).
    2. All code has been properly and consistently formatted either manually or by using the code formatter in the IDE.
    3. All unnecessary dead code (e.g., unused code snippets, unused import statements, etc.) has been removed.
    4. Packages/modules/directories/etc. have been properly used to organize your code. 
    5. Make sure that this final version __actually compiles and runs__. _The course staff need to be able 
       to run your project **on their computer**._
    6. Make sure that __the automated tests are passing__.

- You must have good automated tests (unit tests or blackbox tests) for the majority of your code.


Final Demo
----------

The Final demo meetings will last likely for 1 hour for each team. So plan for this length.

- Briefly introduce the main goal of the project.

- (10 min) Give the course staff a quick overview of the whole project.
   - Describe the __actual__ distribution of the __user stories__ across the the iterations.
   - Summarize the contribution and the roles of each team member. 

- (20 min) Give a demo of your final deliverable showing how it works.
   - Be sure to demonstrate all the features that were implemented (with respect to the user stories).
   - Please mention those things that are not working fully as well.

- (30 min) A walk through of the final code. 
   - This code shall be clean and well-refactored.
   - Each member of the team shall be prepared to talk about some portion of the code.
