Final Presentation
==================

- The final presentation will be held in classroom B2-102 at __6:30pm on Dec 13 (Thursday)__.
- Each team will give a presentation for __15 minutes__ in __English__, and there will be a Q/A session for 5 minutes after each talk.
- One speaker will be chosen for each team, but anyone can answer questions.
 
During the presentation
-----------------------

 - (1 min) Briefly introduce the main goal of the project.
 - (7 min) Demonstrate the key features of your project that were implemented.
 - (7 min) Explain and evaluate XP practices applied by your team.

 
