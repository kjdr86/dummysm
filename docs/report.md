Final Report
------------

- __Due 12/16 11:59pm__, please submit the report to the course LMS system.
- Each team shall submit a final document in PDF format.

- The report needs to address the following:
  1. Brief description of your projct
     - Explain the goals of the project, and the problem that you are trying to solve.
     - If your project extends an existing open source project, explain new features/functionalities.
  2. Architecture and design
     - Explain the major components of your implementation and how they are related.
     - Make clear how your software meets fundamental __design principles__, such as low coupling, high cohesion, etc.
     - If you want to sketch some UML diagrams of main classes, you can do so.
  3. Appendix
     - How to run the tests for your implemented requirements.
     - In case you are doing some manual testing, the instructions for those shall be detailed enough.
     
> Write the report as if you are writing it for new developers who are about to join your project. 
> Include enough details so that they can get started quickly but do not overwhelm them with too much 
> information that would be easier to find out from reading the code.

- Please submit your final report in a readable format (in PDF).


Peer/Self Evaluation
--------------------

- __Due 12/16 11:59pm__, each student __must__ turn in a peer evaluation form.
- Peer evaluations are confidential; your team members will not know what you write about them. Please be honest when you do the evaluations.
- Additionally, you shall write a personal evaluation of your own contributions to the project.
- You shall download the evalutation form from the course LMS system (Homework management), and upload your completed form to LMS as well.
