_**Feel free to change the detailed format for each item below, or to create subpages linked from the main page.**_
_**You should remove this comment from your webpage.**_

Team ``Your_Team_Name``
=======================

Members
-------

[Kyungmin Bae](@kmbae), [Jaeseo Lee](@jlee), [Jia Lee](@jialee17), ...

Experiences
-----------

- person 1
  - Java, Eclipse, ...
- person 2
  - Java, Eclipse, ...


Project Description
-------------------

_**Give a couple of sentences describing this project and what it does.**_ 
_**Describe why you are doing this project and why the project is interesting.**_
(_**You should remove this comment from your webpage.**_)


Weekly Meetings
---------------

Every Monday, 5pm - 6pm

Progress Report
---------------

_**Remarks**_ (_**You should remove this comment from your webpage.**_)
  - _**Actual: actual story points you have spent of the user stories in this iteration: initially, blank.**_
  - _**Estimated: estimated story points of the user stories in the current or future iterations.**_
  - _**You should revise the user stories and estimates for the next iterations for each iteration.**_
  - _**If needed, user stories should be divided into several smaller stories to fit in one iteration, when doing revision.**_
  - _**Considering your new estimates, you should divide the user stories among iterations (which user story is going to be done in which iteration).**_
  
  

### Iteration 1

| actual  | estimated | story | description |
| ------  | --------- | ----- | ----------- |
| 8 units | 5 units   | T_S01 | Allow User to specify thresholds for test duration |
|         | 5 units   | T_S02 | Collect test test information on the fly           |
|         | 5 units   | T_S03 | Display newly failing/passing tests                |



Minutes of Meetings
-------------------

#### Minutes of Meeting held on (PUT DATE HERE)
- Venue: 
- Members Present (with roles): person1 (Moderator), person2 (Author), person3 (Reviewer), person4 (Scribe)
- Agenda:
  ...
- Meeting notes:
  ...
- Deliverables by next meeting:
  ...