package edu.postech.csed332.homework2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonElement;
/**
 * Container class for all the collections (that eventually contain books). The
 * library object is just a container for all collections. A library can be
 * exported to or imported from a file.
 * 
 * The format of the file is of your choice. Again, we strongly encourage using
 * some library to convert between Library objects and string representation.
 */
public final class Library {
	private List<Collection> collections;

	/**
	 * Builds a new, empty library.
	 */
	public Library() { 
		// TODO implement this
		this.collections = new ArrayList();
	}

	/**
	 * Builds a new library and restores its contents from the given file.
	 *
	 * @param fileName the file from where to restore the library.
	 */
	public Library(String fileName) {
		// TODO implement this
		//[{col1},{col2}]
		//File file = new File(fileName);
		this.collections = new ArrayList();
		try
		{
			FileReader fr = new FileReader(fileName);
			BufferedReader br = new BufferedReader(fr);
			String jlib = br.readLine();

			JsonParser parser = new JsonParser();
			JsonArray jcols = parser.parse(jlib).getAsJsonArray();
			for(int i = 0; i < jcols.size(); i++)
			{
				Collection tmp = new Collection("tmp");
				tmp = Collection.restoreCollection(jcols.get(i).toString());
				this.collections.add(tmp);				
			}
			br.close();
		} catch(IOException e) {
			System.out.println(e);
		}
		
	}

	/**
	 * Saved the contents of the library to the given file.
	 *
	 * @param fileName the file where to save the library
	 */
	public void saveLibraryToFile(String fileName) {
		// TODO implement this
		//File file = new File(fileName);
		try
		{
			FileWriter fw = new FileWriter(fileName);
			BufferedWriter bw = new BufferedWriter(fw);
			Gson gson = new Gson();
			JsonArray jlib = new JsonArray();
			for(int i = 0; i < this.collections.size(); i++)
			{
				jlib.add(this.collections.get(i).getStringRepresentation());
			}
			String lib = gson.toJson(jlib);
			bw.write(lib);
			bw.close();
		} catch(IOException e) {
			System.out.println(e);
		}
	}

	/**
	 * Returns the collections contained in the library.
	 *
	 * @return library contained elements
	 */
	public List<Collection> getCollections() {
		// TODO implement this
		return this.collections;
	}

	/**
	 * Return the set of all the books that belong to the given collection in the Library.
	 * Return null if the given collection doesn't exist. 
	 * Note that the name of the collection is unique and the findBooks should go through
	 * all the collections in the hierarchy of the given collection.
	 * Consider the following.
	 * (1) Computer Science is a collection. 
	 * (2) The "Introduction of Computer Science" is a book under Computer Science. 
	 * (3) Software Engineering is a collection under Computer Science. 
	 * (4) The "Software design method" is a book under Software Engineering collection. 
	 * (5) Development Methodology is a collection under Software Engineering.
	 * (6) The "Agile Programming" is a book under Development Methodology.
	 * 
	 * Then, findBooks method for the Computer Science should return a set of these 
	 * three Book objects "Introduction of Computer Science", "Software design method",
	 * and "Agile Programming".
	 * 
	 * @param collection the collection that want to know the belonging books
	 * @return Return the set of the books that belong to the given collection
	 */
	public Set<Book> findBooks(String collection) {
		// TODO implement this
		Set<Book> books = new HashSet<Book>();
		for(int i = 0; i < this.collections.size(); i++)
		{
			Collection ith = this.collections.get(i);
			if(ith.getName() == collection)
			{
				List<Element> ith_el = ith.getElements();
				for(int j = 0; j <ith_el.size(); j++)
				{
					if(ith_el.get(j) instanceof Book)
					{
						Book book = (Book) ith_el.get(j);
						books.add(book);
					} 
					else
					{
						Collection col = (Collection) ith_el.get(j);
						books.addAll(col.getBooks());
					}
				}
				return books;
			}
		}
		return null;
	}

}
