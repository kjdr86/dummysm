package edu.postech.csed332.homework2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonElement;
/**
 * The Collection class represents a single collection. It contains a name of
 * the collection and also has a list of references to every book and every
 * sub-collection in that particular collection.
 * 
 * A collection can be exported to and imported from a string representation.
 * The format of the string is of your choice, but the string representation
 * should have the name of this collection and all elements (books and
 * sub-collections) contained in this collection.
 */
public final class Collection extends Element {
	List<Element> elements;
	private static String name;

	/**
	 * Creates a new collection with the given name.
	 *
	 * @param name the name of the collection
	 */
	public Collection(String name) {
		// TODO implement this
		this.elements = new ArrayList();
		this.name = name;
	}

	/**
	 * Restores a collection from its given string representation.
	 *
	 * @param stringRepresentation the string representation
	 */
	public static Collection restoreCollection(String stringRepresentation) {
		// TODO implement this
		//{"name":"name1","books":[{},{}],"collections":[{},{}]}
		String rname;
		List<Element> relements = new ArrayList();
		
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(stringRepresentation);
		
		rname = element.getAsJsonObject().get("name").getAsString();
		
		JsonArray jbooks = element.getAsJsonObject().get("books").getAsJsonArray();
		JsonArray jcols = element.getAsJsonObject().get("collections").getAsJsonArray();
		for(int i = 0; i < jbooks.size(); i++)
		{
			Book book = new Book(jbooks.get(i).toString());
			relements.add(book);
		}
		for(int i = 0; i < jcols.size(); i++)
		{ 
			Collection tmp = new Collection("tmp");
			relements.add(tmp.restoreCollection(jcols.get(i).toString()));
		}
		Collection rcol = new Collection(rname);
		for(int i= 0; i<relements.size(); i++)
		{
			rcol.addElement(relements.get(i));
		}
		return rcol;
	}

	/**
	 * Returns the string representation of a collection. The string representation
	 * should have the name of this collection, and all elements
	 * (books/subcollections) contained in this collection.
	 *
	 * @return string representation of this collection
	 */
	public String getStringRepresentation() {
		// TODO implement this
		//{"name":"name1","books":[{},{}],"collections":[{},{}]}
		String collection;
		Gson gson = new Gson();
		JsonArray jbooks = new JsonArray();
		JsonArray jcols = new JsonArray();
		//JsonObject obooks = new JsonObject();
		//JsonObject ocols = new JsonObject();
		JsonObject ocol = new JsonObject();
		ocol.addProperty("name", this.name);
		for(int i = 0; i < this.elements.size(); i++)
		{
			if(this.elements.get(i) instanceof Book)
			{
				Book book = (Book) this.elements.get(i);
				jbooks.add(book.getStringRepresentation());
			}
			else
			{
				Collection col = (Collection) this.elements.get(i);
				jcols.add(col.getStringRepresentation());
			}
		}
		ocol.add("books", jbooks);
		ocol.add("collections", jcols);
		collection = gson.toJson(ocol);
		return collection;
		
	}

	/**
	 * Returns the name of the collection.
	 *
	 * @return the name
	 */
	public String getName() {
		// TODO implement this
		return this.name;
	}

	/**
	 * Adds an element to the collection. If parentCollection of given element is
	 * not null, do not actually add, but just return false. (explanation: if given
	 * element is already a part of another collection, you should have deleted the
	 * element from that collection before adding to another collection)
	 *
	 * @param element the element to add
	 * @return true on success, false on fail
	 */
	public boolean addElement(Element element) {
		// TODO implement this
		if(element.getParentCollection() != null)
			return false;
		element.setParentCollection(this);
		this.elements.add(element);
		return true;
	}

	/**
	 * Deletes an element from the collection. Returns false if the collection does
	 * not have this element. Hint: Do not forget to clear parentCollection of given
	 * element
	 *
	 * @param element the element to remove
	 * @return true on success, false on fail
	 */
	public boolean deleteElement(Element element) {
		// TODO implement this
		for(int i = 0; i < this.elements.size(); i++)
		{
			if(this.elements.contains(element))
			{
				element.setParentCollection(null);
				elements.remove(i);
				i--;
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the list of elements.
	 * 
	 * @return the list of elements
	 */
	public List<Element> getElements() {
		// TODO implement this
		return this.elements;
	}
	
	public Set<Book> getBooks()
	{
		Set<Book> books = new HashSet<Book>();
		for(int i = 0; i < this.elements.size(); i++)
		{
			if(this.elements.get(i) instanceof Book)
			{
				Book book = (Book) this.elements.get(i);
				books.add(book);
			}
			else
			{ 
				Collection col = (Collection) this.elements.get(i);
				books.addAll(col.getBooks());
			}
		}
		return books;
	}

	/**
	 * Return the books that the given author wrote, by him/herself or as co-author,
	 * in this Collection. Return null if the given author didn't write anything. 
	 * If the author wrote several books, return all the books. 
	 *
	 * @param author the author that want to find
	 * @return Return the book titles that the given author wrote
	 */
	public Set<Book> findTitle(String author) {
		// TODO implement this
		Book herbook;
		Set<Book> herbooks = new HashSet<Book>();
		for(int i = 0; i < this.elements.size(); i++)
		{
			if(this.elements.get(i) instanceof Book)//element is a book
			{
				herbook = (Book) this.elements.get(i);
				if(herbook.getAuthor().contains(author))
					herbooks.add(herbook);
			}
			else//element is a sub collection
			{
				Collection sub = (Collection) this.elements.get(i);
				if(sub.findTitle(author) != null)
					herbooks.addAll(sub.findTitle(author));
			}
		}
		
		if(herbooks.isEmpty())
			return null;
		else
			return herbooks;
	}

}
