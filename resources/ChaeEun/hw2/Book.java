package edu.postech.csed332.homework2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonElement;


/**
 * A book will contain only the title and the author(s).
 * 
 * A book can be exported to and imported from a string representation. 
 * The format of the string is of your choice, but by using the format, 
 * you should be able to construct a book object.
 * 
 * Do not reinvent the wheel. We strongly recommend that 
 * you use some library for XML, JSON, YML, or similar format 
 * instead of writing your own parsing code.
 * 
 * While using the library requires adding it to pom.xml, 
 * it will be overall faster for you, likely result in less buggy code, 
 * and you will get to learn more about Maven and Java.
 */
public final class Book extends Element {
    private String title;
    private Set<String> authors;

    /**
     * Builds a book with the given title and authors.
     *
     * @param title the title of the book
     * @param authors the authors of the book
     */
    public Book(String title, Set<String> authors) {
		// TODO implement this
    	this.title = title;
    	this.authors = authors;
    }

    /**
     * Builds a book from the string representation, 
     * which contains the title and authors of the book. 
     *
     * @param stringRepresentation the string representation
     */
    public Book(String stringRepresentation) {
		// TODO implement this
    	//{"title":"title1","authors":"["author1","author2"]"}
    	JsonParser parser = new JsonParser();
    	JsonElement element = parser.parse(stringRepresentation);
    	//"title1"
    	this.title = element.getAsJsonObject().get("title").getAsString();
    	this.authors = new HashSet<String>();
    	//"[\"author1\",\"author2\"]"
    	JsonArray jauthors = element.getAsJsonObject().get("authors").getAsJsonArray();
    	for(int i = 0; i < jauthors.size(); i++)
    	{
    		String author = jauthors.get(i).getAsString();
    		this.authors.add(author);
    	}
    }

    /**
     * Returns the string representation of the given book.
     * The representation contains the title and authors of the book.
     *
     * @return the string representation
     */
    public String getStringRepresentation() {
		// TODO implement this
    	//{"title":"a","authors":["Cho","Lee"]}
		String book;
		JsonObject obook = new JsonObject();
		Gson gson = new Gson();
		obook.addProperty("title", this.title);
		JsonArray jauthors = new JsonArray();
		List<String> lauthors = new ArrayList();
		lauthors.addAll(this.authors);
		for (int i = 0; i < this.authors.size(); i++) {
			jauthors.add(lauthors.get(i));
		}
		obook.add("authors", jauthors);
		book = gson.toJson(obook);
		return book;
    }

    /**
     * Returns all the collections that this book belongs to directly and indirectly.
     * Consider the following. 
     * (1) Computer Science is a collection. 
     * (2) Operating Systems is a collection under Computer Science. 
     * (3) The Linux Kernel is a book under Operating System collection. 
     * Then, getContainingCollections method for The Linux Kernel should return a list 
     * of these two collections (Computer Science, Operating System), starting from 
     * the top-level collection.
     *
     * @return the list of collections
     */
    public List<Collection> getContainingCollections() {
		// TODO implement this
    	List<Collection> collections = new ArrayList<Collection>();
    	Collection parent = this.getParentCollection();
    	while(parent != null)
    	{
    		collections.add(0,parent);
    		parent = parent.getParentCollection();
    	}
    	return collections;
    }

    /**
     * Returns the title of the book.
     *
     * @return the title
     */
    public String getTitle() {
		// TODO implement this
    	return this.title;
    }

    /**
     * Returns the authors of the book.
     *
     * @return the authors
     */
    public Set<String> getAuthor() {
		// TODO implement this
    	return this.authors;
    }
    
}
