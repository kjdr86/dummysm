package edu.postech.csed332.homework3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;

/**
 * 
 * An instance of this class represents a even/odd Sudoku puzzle. This class must be 
 * immutable. That is, you should define a constructor to create a Sudoku object with 
 * all the necessary information for a concrete even/odd Sudoku puzzle.
 * 
 * You can freely add member variables and methods to implement this class.
 * 
 */
public class Sudoku {
   //TODO implement this
   ISolver solver;
   int numVar;
   int size;
   int block_size;
   int unfixed_cell_num;
   List<int[]> unfixed_sols;
   int[][] init;
   
   Sudoku(int newVar, int size, int block_size, int unfixed_cell_num, int[][] init)
   {
       this.numVar = newVar;//# of variables
       this.solver = SolverFactory.newDefault();//default solver obtain
       this.solver.newVar(newVar);//# of variables for solver
       this.size = size; 
       this.block_size = block_size;      
       this.unfixed_cell_num = unfixed_cell_num;
       this.unfixed_sols = new ArrayList<int[]>();
       this.init = init;
   }
   
   /**
    * Returns a set of Solution instance for the given sudoku puzzle.
    * 
    * @return the set of solutions
    * @throws ContradictionException 
    * @throws TimeoutException  
    */
   public Set<Solution> solve() throws ContradictionException, TimeoutException{
      //TODO implement this 
      Set<Solution> sols = new HashSet<Solution>();
       
         while(true)
         {
            Solution solution = new Solution(this.size, this.block_size, this.solver, this.init);
            
            solution.rules();
            
            if(solution.solver.isSatisfiable())
            {
               solution.make_sol();
               int[] sol;
               sol = new int[this.unfixed_cell_num];
               int count = 0;
               for(int r = 1; r <= size; r++)
               {
                  for(int c = 1; c <= size; c++)
                  {
                     for(int v = 1; v <= size; v++)
                     {
                        if(this.solver.model(variable(r,c,v)) && (isEven(r,c) || isOdd(r,c)))//unfixed cell && value is v
                        {
                           sol[count++] = -variable(r,c,v);
                        }
                     }      
                  }
               }   
             
               this.unfixed_sols.add(sol);
               this.solver.clearLearntClauses();//delete clauses which are for previous solution
               this.pre_sols_solver();
               
               sols.add(solution);
            }
            else
            {
               System.out.println("No More Solutions");
               break;//no more solution
            }
         }
         
         return sols;
   } 
   
   void pre_sols_solver() throws ContradictionException
   {
      for(int i = 0; i < this.unfixed_sols.size(); i++)
      {
         int sol[] = this.unfixed_sols.get(i);
         VecInt vi = new VecInt();
         for(int j = 0; j < sol.length; j++)
         {
            vi.push(sol[j]);
         }
         this.solver.addClause(vi);
      }
   } 
   
   int variable(int row, int col, int v)
   {
      return 100*row + 10*col + v;
   }
   
   /**
    * Returns true if a given cell must contain an even number.
    * 
    * @param row row number
    * @param column column number 
    * @return true if a cell (row, column) must be filled by an even number; otherwise, return false.
    */
   public Boolean isEven(int row, int column) {
      //TODO implement this
      if(this.init[row][column] == 0)
         return true;
      else
         return false;
   }
   
   public Boolean isOdd(int row, int column)
   {
	   if(this.init[row][column] == -1)
		   return true;
	   else
		   return false;
   }
   
   /**
    * Return the initial value of a cell, given an even/odd Sudoku puzzle.
    * If there is no value to get (e.g. out of range, or empty), return null.
    * 
    * @param row row number
    * @param column column number
    * @return A value of a cell, if it is non-empty
    */
   public Integer getValue(int row, int column) {
      //TODO implement this
      if(row <=0 || row > this.size || column <= 0 || column > this.size || this.init[row][column] == 0 || this.init[row][column] == -1)
         return null;
      else
      {
         Integer i;
         i = this.init[row][column];
         return i;
      }
   }
   
}