package edu.postech.csed332.homework3;

import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;
import java.io.*;
import java.util.*;

/**
 * Receives a sudoku puzzle as a file by the command line argument,
 * solves the puzzle, and makes a file for each solution
 * 
 * For example, if you received an input sudoku file "A" and the 
 * solver gives 3 solutions for the puzzle, the output files 
 * "A_1.solution", "A_2.solution" and "A_3.solution" will be made.
 *
 */
public class Game {   
   //TODO implement this
   int size = 9;
   int block_size = 3;
   int[][] init;//value -> value, even empty cell -> 0, odd empty cell -> -1
   int sol_count;//# of solution 
   int unfixed_cell_num;//# of empty cell
   int numVar;
   
   Game()
   {      
       this.init = new int[size + 1][size + 1];
       this.sol_count = 0;
       this.unfixed_cell_num = 0;
       this.numVar = size*size*size;
   }
   /**
    * @param args name of the input file for a sudoku puzzle
    * @throws TimeoutException 
    * @throws ContradictionException 
    */ 
   public static void main(String[] args) throws ContradictionException, TimeoutException 
   {
      
      Game game = new Game();
      
      int ir = 1; 
      int ic = 1;
      try
      { 
          File file1 = new File(args[0]);
          FileReader fr = new FileReader(file1);
          BufferedReader br = new BufferedReader(fr);
          String line = "";
          while((line = br.readLine())!= null)
          {
             for(int i = 0; i< line.length();i++)
              {
                if(line.charAt(i) == '*')
                  {
                   game.init[ir][ic] = 0;
                      ic++;
                      game.unfixed_cell_num++;
                  }
                  else if(line.charAt(i) == '.')
                  {
                     game.init[ir][ic] = -1;
                      ic++;
                      game.unfixed_cell_num++;
                  }
                  else
                  {
                     game.init[ir][ic] = Character.getNumericValue(line.charAt(i));
                      ic++;
                  }
               }
                  ir++;
                  ic = 1;
               }
               fr.close();
               br.close();
               
          }catch(IOException e)
          {   
              System.out.println(e);
          }
      
      Sudoku sudoku = new Sudoku(game.numVar, game.size, game.block_size, game.unfixed_cell_num, game.init);
       
      Set<Solution> solutions = sudoku.solve();
        
      if(!solutions.isEmpty())
         game.sol_count++;//found one more solution

      Iterator<Solution> it_sols = solutions.iterator();
      
      try
      {
          while(it_sols.hasNext())
          {
        	  Solution tmp_sol = it_sols.next();
        	  
        	  int dot = args[0].lastIndexOf(".");
          
        	  String outfilename = args[0].substring(0, dot) + "_" + String.valueOf(game.sol_count++) + ".solution";
        	  //String outfilename = args[0].substring(0, dot) + "_" + String.valueOf(game.sol_count++) + ".txt";
        	  File file2 = new File(outfilename);
        	  FileWriter fw = new FileWriter(file2);
        	  BufferedWriter bw = new BufferedWriter(fw);
       
        	  if(file2.isFile() && file2.canWrite())
        	  {
        		  for(int r = 1; r <= game.size; r++)
        		  {
        			  String s = "";
        			  for(int c = 1; c <= game.size; c++)
        			  {
        				  s = s + String.valueOf(tmp_sol.getValue(r, c));
        			  }
        			  bw.write(s);
        			  if(r != game.size)
        				  bw.newLine();
        		  }
        	  }
        	  bw.close();
        	  fw.close();
          }
      }catch(IOException e)
      {
    	  ;
      }
   
   }         
}