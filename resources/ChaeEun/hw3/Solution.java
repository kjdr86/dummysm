package edu.postech.csed332.homework3;

import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;
import java.util.*;
/**
 * 
 * An instance of this class represents a single solution for a sudoku puzzle
 * 
 * You can freely add member variables and methods to implement this class
 * 
 */
public class Solution {
   //TODO implement this
   int size;
   int block_size;
   ISolver solver;
   int numVar;
   int[][] sol;
   int[][] init;
   
   Solution(int size, int block_size, ISolver solver, int[][] init)
   {
      this.size = size;
      this.block_size = block_size;
      this.solver = solver;
      this.sol = new int[size + 1][size + 1];
      this.init = init;
   }
   
   /**
    * Returns a value of a cell in the solution. If the input is out of range, return null.
    * 
    * For example, in the example puzzle in the homework description, cell (1,3) will be 
    * filled by number 8 by the solution. In this case, getValue(1,3) should return 8.
    * 
    * @param row row number
    * @param column column number
    * @return Value of a (row, column) cell, provided by a given solution
    */
   public Integer getValue(int row, int column) {
      //TODO implement this
      if(row <= 0 || row > this.size || column <= 0 || column > this.size)
         return null;
      else
      {
         Integer i = new Integer(sol[row][column]);
         return i;
      }
   }
   
   int variable(int row, int col, int v)
   {
      return 100*row + 10*col + v;
   }
 
   //at least one number per one cell
   void cell_solver() throws ContradictionException
   {
      for(int r = 1; r <= size; r++)
       {
         for(int c = 1; c <= size; c++)
         {
            VecInt vi = new VecInt();
               for(int v = 1; v <= size; v++)
               {
                  vi.push(variable(r,c,v));
               }
               this.solver.addClause(vi);
            }
         }
      } 

   //already fixed num & even & odd
   void num_solver() throws ContradictionException
   {
      for(int r = 1; r <= size; r++)
      {
         for(int c = 1; c <= size; c++)
         {
            //it most be odd #
               if(init[r][c] == -1)
               {
                  int n = 1;
                  VecInt vi = new VecInt();
                  for(int v = 1; v <= (size+1)/2; v++)
                  {
                     vi.push(variable(r,c,n));
                     n = n + 2;
                  }
                  solver.addClause(vi);
               } 
               //it most be even #
               else if(init[r][c] == 0)
               {
                  int n = 2;
                  VecInt vi = new VecInt();
                  for(int v = 1; v <= size/2; v++)
                  {
                     vi.push(variable(r,c,n));
                     n = n + 2;
                  }
                  solver.addClause(vi);
               } 
               //already defined
               else
               {
                  VecInt vi = new VecInt();
                  vi.push(variable(r, c, init[r][c]));
                  solver.addClause(vi);
               }
         }
      }
    }
      

   int blockVariable(int br, int bc, int r, int c, int v) 
   {
      return variable((br - 1) * block_size + r, (bc - 1) * block_size + c, v);
   }   
      
   //there can't be same numbers in each block
   void block_solver() throws ContradictionException
   {
      for(int br = 1; br <= block_size; br++)
      {
         for(int bc = 1; bc <= block_size; bc++)
         {
            for(int v = 1; v <= size; v++)
               {
               for(int i1 = 1; i1 <= size -1; i1++)
               {
                  for(int i2 = i1 + 1; i2 <= size; i2++)
                  {
                     int r1 = (i1 - 1) / block_size + 1;
                     int c1 = (i1 - 1) % block_size + 1;
                     int r2 = (i2 - 1) / block_size + 1;
                     int c2 = (i2 - 1) % block_size + 1;
                     
                     VecInt vi = new VecInt();
                     vi.push(-blockVariable(br, bc, r1, c1, v));
                     vi.push(-blockVariable(br, bc, r2, c2, v));
                     solver.addClause(vi);
                  }
               }
               }
         }
      }
   }
         
   //there can't be same numbers in each rows
   void row_solver() throws ContradictionException
   {
      for(int r = 1; r <= size; r++)
      {
         for(int v = 1; v <= size; v++)
         {
            for(int c1 = 1; c1 <= size-1; c1++)
               {
               for(int c2 = c1 + 1; c2 <= size; c2++)
               { 
                  VecInt vi = new VecInt();
                  //vi.clear();
                  vi.push(-variable(r,c1,v));
                  vi.push(-variable(r,c2,v));
                  solver.addClause(vi);
               }
               }
            }
         }
   }
      
   //there can't be same numbers in each cols
   void col_solver() throws ContradictionException
   {
      for(int c = 1; c <= size; c++)
      {
         for(int v= 1; v <= size; v++)
         {
            for(int r1 = 1; r1 < size; r1++)
               {
               for(int r2 = r1 + 1; r2 <= size; r2++)
               {
                  VecInt vi = new VecInt();
                  vi.push(-variable(r1,c,v));
                  vi.push(-variable(r2,c,v));
                  solver.addClause(vi);
               }
               }
         }
      }
   }
      
   void rules() throws ContradictionException
   {
      num_solver();
      cell_solver();
      row_solver();
      col_solver();
      block_solver();
   }

   void make_sol()
   {
      for(int r = 1; r <= this.size; r++)
      {
         for(int c = 1; c <= this.size; c++)
         {
            for(int v = 1; v <= this.size; v++)
            {
               if(this.solver.model(variable(r,c,v)))
               {
                  sol[r][c] = v;
               }
            }
         }
      }
   }
}