package edu.postech.csed332.homework1;

/**
 * The interest of HighInterest account is 1% per day. For example, one person puts 100
 * dollars in his HighInterest Account(At Day 1). After 10 days(Day 11), 
 * the balance of the account will be 100*(1.01)^10.
 * The balance of HighInterest account should always bigger or equal to 1000. 
 */
class HighInterest implements Account {
//TODO implement this
	String name;//owner name
	double bal;//balance
	int accnum;//account number
	public HighInterest(String ownername, double initial, int AccountNumber)
	{
		name = ownername;
		bal = initial;
		accnum = AccountNumber;
	}
	/**
	 *  Returns the account number 
	 *  
	 *  @return the account number
	 */
	public double getAccountNumber()
	{
		return accnum;
	}
	/**
	 *  Returns the account balance 
	 *  
	 *  @return the balance
	 */
	public double getBalance()
	{
		return bal;
	}
	/**
	 *  Returns the account owner name 
	 *  
	 *  @return the owner name
	 */
	public String getOwner()
	{
		return name;
	}
	
	/**
	 *  This function is for updating the balance according to the interest rate and 
	 *  	elapsed date. 
	 *  
	 *  @param elapsedDate elapsed date
	 */
	public void updateBalance(int elapsedDate)
	{
		for(int i = 1; i <= elapsedDate; i++)
		{
			bal = bal * 1.01;
		}
	}
	
	/**
	 *  Add as much money as a given amount factor to the given account 
	 *  
	 *  @param amount deposit amount
	 */
	public void deposit(double amount)
	{
		bal = bal + amount;
	}
	
	/**
	 *  Withdraw as much money as a given amount factor to the given account. 
	 *  	You have to use exception handler to handle the case that the balance 
	 *  of account is smaller than the amount of money that you want to withdraw.
	 *  Please use NegativeException we gave you.
	 *  
	 *  @param amount withdraw amount 
	 */
	public void withdraw(double amount) throws NotEnoughException
	{
		if(amount > bal || (bal - amount) < 1000)
		{
			throw new NotEnoughException();
		}
	}
}
