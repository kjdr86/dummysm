package edu.postech.csed332.homework2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;

/**
 * Container class for all the collections (that eventually contain books). The
 * library object is just a container for all collections. A library can be
 * exported to or imported from a file.
 * 
 * The format of the file is of your choice. Again, we strongly encourage using
 * some library to convert between Library objects and string representation.
 */
public final class Library {
	private List<Collection> collections;

	/**
	 * Builds a new, empty library.
	 */
	public Library() {
		// TODO implement this
		this.collections = new ArrayList<Collection>();
	}

	/**
	 * Builds a new library and restores its contents from the given file.
	 *
	 * @param fileName the file from where to restore the library.
	 */

	public Library(String fileName) {
		// TODO implement this
		//Reference: http://jeong-pro.tistory.com/69
		
		String str = "";
	    try {
	    	//Open
	    	File file = new File(fileName);
	    	FileReader filereader = new FileReader(file);
	    	BufferedReader bufreader = new BufferedReader(filereader);
	    	//Read
	    	str = bufreader.readLine();
	    	//Close
	    	bufreader.close();
	    	filereader.close(); 
	    }
	    catch (FileNotFoundException e) {
	    	System.out.println(e);
	    }
	    catch (IOException e) {
	    	System.out.println(e);
	    }
	    
	    Collection root = Collection.restoreCollection(str);
	    this.collections = this.collectionDfs(root);
	    
	}
	//Recursive function that finds all collections by dfs
	private List<Collection> collectionDfs(Collection root) {
		List<Collection> res= new ArrayList<Collection>();
		res.add(root);
		
		for(Element e : root.elements) {
			if(e.getClass().getName().contains("Collection")) 
				res.addAll(this.collectionDfs((Collection) e));
		}
		return res;
	}
	//Recursive function that finds all books by dfs
	private static Set<Book> bookDfs(Collection root) {
		Set<Book> res= new HashSet<Book>();
		
		for(Element e : root.elements) {
			if(e.getClass().getName().contains("Book")) 
				res.add((Book) e);
			else
				res.addAll(bookDfs((Collection) e));
		}
		return res;
	}
	/**
	 * Saved the contents of the library to the given file.
	 *
	 * @param fileName the file where to save the library
	 */
	public void saveLibraryToFile(String fileName) {
		// TODO implement this
		Collection root = null;
		
		//Find roots
		for(Collection c : this.collections) {
			if(c.getParentCollection() == null) {
				root = c;
				break;
			}
		}
		
		//File IO
		File file = new File(fileName);
		try {
			BufferedWriter bufWriter = new BufferedWriter(new FileWriter(file));
			
			if(file.isFile() && file.canWrite()) {
				String str = root.getStringRepresentation();
				bufWriter.write(str);
				bufWriter.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}

	/**
	 * Returns the collections contained in the library.
	 *
	 * @return library contained elements
	 */
	public List<Collection> getCollections() {
		// TODO implement this
		return this.collections;
	}

	/**
	 * Return the set of all the books that belong to the given collection in the Library.
	 * Return null if the given collection doesn't exist. 
	 * Note that the name of the collection is unique and the findBooks should go through
	 * all the collections in the hierarchy of the given collection.
	 * Consider the following.
	 * (1) Computer Science is a collection. 
	 * (2) The "Introduction of Computer Science" is a book under Computer Science. 
	 * (3) Software Engineering is a collection under Computer Science. 
	 * (4) The "Software design method" is a book under Software Engineering collection. 
	 * (5) Development Methodology is a collection under Software Engineering.
	 * (6) The "Agile Programming" is a book under Development Methodology.
	 * 
	 * Then, findBooks method for the Computer Science should return a set of these 
	 * three Book objects "Introduction of Computer Science", "Software design method",
	 * and "Agile Programming".
	 * 
	 * @param collection the collection that want to know the belonging books
	 * @return Return the set of the books that belong to the given collection
	 */
	
	public Set<Book> findBooks(String collection) {
		// TODO implement this
		//Search if there is a collection named "collection"
		Collection root = null;
		for (Collection c : this.collections) {
			if(c.getName().equals(collection)) {
				root = c;
				break;
			}
		}
		
		if(root == null)
			return null;
		else
			return bookDfs(root);
	}
	
}