package edu.postech.csed332.homework2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

/**
 * The Collection class represents a single collection. It contains a name of
 * the collection and also has a list of references to every book and every
 * sub-collection in that particular collection.
 * 
 * A collection can be exported to and imported from a string representation.
 * The format of the string is of your choice, but the string representation
 * should have the name of this collection and all elements (books and
 * sub-collections) contained in this collection.
 */
public final class Collection extends Element {
	List<Element> elements;
	private String name;

	/**
	 * Creates a new collection with the given name.
	 *
	 * @param name the name of the collection
	 */
	public Collection(String name) {
		// TODO implement this
		this.name = name;
		this.elements = new ArrayList<Element>();
		this.setParentCollection(null);
	}

	/**
	 * Restores a collection from its given string representation.
	 *
	 * @param stringRepresentation the string representation
	 */
	public static Collection restoreCollection(String str) {
		// TODO implement this
		
		//Null collection
		//Example; Collection:Computer Science
		//		 ; ==> Computer Science
		if(str.contains("(") == false) {
			str = str.substring(11);
			return new Collection(str);
		}
		
		///System.out.println("1 " + str);
		//Parent collection
		//Name it
		//Example; Collection:Computer Science(Collection:Operating System)
		//		 ; ==> Computer Science
		int nameIdx1 = str.indexOf(":");
		int nameIdx2 = str.indexOf("(");
		String name = str.substring(nameIdx1 + 1, nameIdx2);
		///System.out.println("2 " + name);
		Collection res = new Collection(name);
		
		//Remove the name of it and leave only the children nodes
		//Example; Collection:Computer Science(Collection:Operating System)
		//		 ; ==> (Collection:Operating System)(...)
		str = str.substring(nameIdx2);
		///System.out.println("3 " + name);
		//Restore children
		while(str.length() > 0) {
			int parIdx1 = 0;	//It's always 0
			int parIdx2 = 0;
			
			//Find the pair parenthesis of nodeIdx1 with stack
			@SuppressWarnings("rawtypes")
			Stack st = new Stack();
			int i;
			for(i = parIdx1; i < str.length(); i++) {
				if(str.charAt(i) == '(') {
					st.push(str.charAt(i));
				}
				else if (str.charAt(i) == ')') {
					if((char) st.peek() == '(') {
						st.pop();
						
						// If stack is empty, this is the pair
						if (st.empty()) {
							parIdx2 = i;
							break;
						}
					}
				}
			} //for
			
			if(parIdx2 == 0) {
				///System.out.println("Parenthesis are not paired");
				return null;
			}
			
			//Parse only one node and remove parenthesizes
			//Example; (Book:Agile Programming;Dahyun Kang,Youngsuk Kim)(....)(....)
			//		 ;  ==> Book:Agile Programming;Dahyun Kang,Youngsuk Kim
			String node = str.substring(parIdx1 + 1, parIdx2);
			///System.out.println("4 " + node);
			
			//If there is no "Collection:" ==> it is 100% a book
			//Example; Book:Agile Programming;Dahyun Kang,Youngsuk Kim
			//		 ;  ==> Agile Programming;Dahyun Kang,Youngsuk Kim
			if(node.contains("Collection:") == false) {
				///System.out.println("5 " + node);
				Book newBook = new Book(node);
				res.addElement(newBook);
			}
			//It is a sub collection
			//Example; Collection:Development Methodology(...)((..)()(...))(...)
			//		 ;  ==> Agile Programming;Dahyun Kang,Youngsuk Kim
			else {
				///System.out.println("6" + node);
				res.addElement(restoreCollection(node));
			}
			///System.out.println("7 " + str);
			str = str.substring(parIdx2+1);		
			///System.out.println("8 " + str);
		}
		return res;
	}

	/**
	 * Returns the string representation of a collection. The string representation
	 * should have the name of this collection, and all elements
	 * (books/subcollections) contained in this collection.
	 *
	 * @return string representation of this collection
	 */
	public String getStringRepresentation() {
		// TODO implement this		
		
		// when number of element is 0, then don't make ()
		String res = "Collection:" + this.name;
		
		if(this.elements.isEmpty()) {
			return res;
		}
		
		//Every elements is enclosed with ( and ) 
		for (Element e : this.elements) {
			res += "(";
			if(e.getClass().getName().contains("Collection")) 
				res += ((Collection) e).getStringRepresentation();
			else
				res += ((Book) e).getStringRepresentation();
			res += ")";
		}
		return res;
	}

	/**
	 * Returns the name of the collection.
	 *
	 * @return the name
	 */
	public String getName() {
		// TODO implement this
		return this.name;
	}

	/**
	 * Adds an element to the collection. If parentCollection of given element is
	 * not null, do not actually add, but just return false. (explanation: if given
	 * element is already a part of another collection, you should have deleted the
	 * element from that collection before adding to another collection)
	 *
	 * @param element the element to add
	 * @return true on success, false on fail
	 */
	public boolean addElement(Element element) {
		// TODO implement this
		//If the element is already someone's child
		if(element.getParentCollection() != null)
			return false;
		
		element.setParentCollection(this);
		this.elements.add(element);
		return true;
	}

	/**
	 * Deletes an element from the collection. Returns false if the collection does
	 * not have this element. Hint: Do not forget to clear parentCollection of given
	 * element
	 *
	 * @param element the element to remove
	 * @return true on success, false on fail
	 */
	public boolean deleteElement(Element element) {
		// TODO implement this
		//If the element is not found
		if(this.elements.contains(element) == false)
			return false;
		
		element.setParentCollection(null);
		this.elements.remove(element);
		return true;
	}

	/**
	 * Returns the list of elements.
	 * 
	 * @return the list of elements
	 */
	public List<Element> getElements() {
		// TODO implement this
		return this.elements;
	}

	/**
	 * Return the books that the given author wrote, by him/herself or as co-author,
	 * in this Collection. Return null if the given author didn't write anything. 
	 * If the author wrote several books, return all the books. 
	 *
	 * @param author the author that want to find
	 * @return Return the book titles that the given author wrote
	 */
	public Set<Book> findTitle(String author) {
		// TODO implement this
		Set<Book> res = new HashSet<Book>();
		
		//If the element is a collection,	call a recursive function
		//If the element is a book,			add the title
    	for (Element e : this.elements) {
    		if(e.getClass().getName().contains("Collection")) {
    			Set<Book> subRes = ((Collection) e).findTitle(author);
    			if(subRes != null)
    				res.addAll( ((Collection) e).findTitle(author) );    			
    		}
    		else {
    			if(((Book) e).getAuthor().contains(author)) {
    				res.add((Book) e); 
    			}
    		}
    	}		
		return res.isEmpty() ? null : res;
	}

}