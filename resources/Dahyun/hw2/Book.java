package edu.postech.csed332.homework2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.lang.String;

/**
 * A book will contain only the title and the author(s).
 * 
 * A book can be exported to and imported from a string representation. 
 * The format of the string is of your choice, but by using the format, 
 * you should be able to construct a book object.
 * 
 * Do not reinvent the wheel. We strongly recommend that 
 * you use some library for XML, JSON, YML, or similar format 
 * instead of writing your own parsing code.
 * 
 * While using the library requires adding it to pom.xml, 
 * it will be overall faster for you, likely result in less buggy code, 
 * and you will get to learn more about Maven and Java.
 */
public final class Book extends Element {
    private String title;
    private Set<String> authors;

    /**
     * Builds a book with the given title and authors.
     *
     * @param title the title of the book
     * @param authors the authors of the book
     */
    public Book(String title, Set<String> authors) {
		// TODO implement this
    	this.title = title;
    	this.authors = authors;
    	this.setParentCollection(null);
    }

    /**
     * Builds a book from the string representation, 
     * which contains the title and authors of the book. 
     *
     * @param stringRepresentation the string representation
     */
    public Book(String stringRepresentation) {
		// TODO implement this
    	stringRepresentation = stringRepresentation.substring(5);

    	//Set name
    	String[] titleStr = stringRepresentation.split(";");
    	this.title = titleStr[0];
	
    	if(titleStr.length == 1)
    		this.authors = null;
    	else {
			//Remaining are authors
	    	String[] authorsStr = titleStr[1].split(",");
	    	this.authors = new HashSet<String>(Arrays.asList(authorsStr));
    	}
    	this.setParentCollection(null);
    }

    /**
     * Returns the string representation of the given book.
     * The representation contains the title and authors of the book.
     *
     * @return the string representation
     */
    public String getStringRepresentation() {
		// TODO implement this
    	String res = "Book:" + this.title + ";";
    	//Split authors
    	for (String s : this.authors) {
    		res += (s + ",");
    	}
    	//Remove the last ","
    	res = res.substring(0, res.length() - 1);
    	return res;
    }

    /**
     * Returns all the collections that this book belongs to directly and indirectly.
     * Consider the following. 
     * (1) Computer Science is a collection. 
     * (2) Operating Systems is a collection under Computer Science. 
     * (3) The Linux Kernel is a book under Operating System collection. 
     * Then, getContainingCollections method for The Linux Kernel should return a list 
     * of these two collections (Computer Science, Operating System), starting from 
     * the top-level collection.
     *
     * @return the list of collections
     */
    public List<Collection> getContainingCollections() {
		// TODO implement this
    	List<Collection> res = new ArrayList<Collection>();
    	
    	//Recursively find the parent until the end
    	Collection it = this.getParentCollection();
    	while(it.getParentCollection() != null) {
    		res.add(it);
    		it = it.getParentCollection();
    	}
    	res.add(it);
    	Collections.reverse(res);
    	return res;
    }

    /**
     * Returns the title of the book.
     *
     * @return the title
     */
    public String getTitle() {
		// TODO implement this
    	return this.title;
    }

    /**
     * Returns the authors of the book.
     *
     * @return the authors
     */
    public Set<String> getAuthor() {
		// TODO implement this
    	return this.authors;
    }
    
}
