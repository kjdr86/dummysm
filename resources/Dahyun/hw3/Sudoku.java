package edu.postech.csed332.homework3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;


/**
 * 
 * An instance of this class represents a even/odd Sudoku puzzle. This class must be 
 * immutable. That is, you should define a constructor to create a Sudoku object with 
 * all the necessary information for a concrete even/odd Sudoku puzzle.
 * 
 * You can freely add member variables and methods to implement this class.
 * 
 */
public class Sudoku {
	//TODO implement this
	private int cell[][];
	private boolean isEven[][];
	private boolean solvable;
	private ISolver solver;
	final List<Integer> evenNums = new ArrayList<Integer>(Arrays.asList(2, 4, 6, 8));
	final List<Integer> oddNums = new ArrayList<Integer>(Arrays.asList(1, 3, 5, 7, 9));
	
	/**
	 * Input the string into a Sudoku object
	 * 
	 * @param str 9*9 input string
	 * @throws WrongFormatException 
	 */
	public Sudoku(List<String> str) throws WrongFormatException {
		cell = new int[10][10];
		isEven = new boolean[10][10];
		this.solvable = true;
		try{
			str2cell(str);
		} catch (WrongFormatException e) {
			throw e;
		}
		this.solver = SolverFactory.newDefault();
		this.solver.newVar(10 * 10 * 10);
		
		/* For Debug purpose
		int i, j;
		String s = "";
		String v = "";
		for(i = 0 ; i <= 9 ; i++) {
			for(j = 0 ; j <= 9 ; j++) {
				s += Integer.toString(cell[i][j]) + " ";
				v += Boolean.toString(isEven[i][j]) + " ";
			}
			System.out.println(s);
			System.out.println(v);
			s = "";
			v = "";
		}
		*/
	}
	/**
	 * Parse the string and store it into 9*9 cells
	 * 
	 * @param str 9*9 input string
	 */
	private void str2cell(List<String> str) throws WrongFormatException {
		try {
			for(int i = 1 ; i <= 9 ; i++) {
				for(int j = 1 ; j <= 9 ; j++) {
					char c = str.get(i - 1).charAt(j - 1);
					
					if(c == '.') {	//'.' is odd
						this.cell[i][j] = -1;
						this.isEven[i][j] = false;
					}
					else if (c == '*') {	//'*' is even
						this.cell[i][j] = -1;
						this.isEven[i][j] = true;
					}
					else if ('1' <= c && c <= '9'){
						this.cell[i][j] = Character.getNumericValue(c);
						this.isEven[i][j] = (this.cell[i][j] & 1) == 0;
					}
					else
						throw new WrongFormatException("There is an invalid character : "+ Character.toString(c));
				}	
			}
		} catch (IndexOutOfBoundsException e) {
			throw new WrongFormatException("The given file is not 9*9 character format.");
		}
	}	
	
	/**
	 * Returns b * (num) clause
	 * 
	 * @param i index number
	 * @param j index number
	 * @param num a literal
	 * @param b boolean applied to the all numbers
	 * @return the b * (num) clause
	 */
	private int[] i2l(int i, int j, int num, boolean b) {
		//If b is true, return positive literal and vice versa
		int temp[] = new int[1];
		temp[0] = b ? 100 * i + 10 * j + num : -(100 * i + 10 * j + num);
		return temp;
	}
	
	/**
	 * Returns b * (l[0] | l[1] | ... | l[n-1]) clause
	 * 
	 * @param i index number
	 * @param j index number
	 * @param list the list of numbers
	 * @param b boolean applied to the all numbers
	 * @return the b * (l[0] | l[1] | ... | l[n-1]) clause
	 */
	private int[] list2clause(int i, int j, List<Integer> list, boolean b) {
		int clause[] = new int[list.size()];
		int k;
		
		//Let me just not use Jdk 1.8 expression
		for(k = 0 ; k < list.size(); k++) {
			clause[k] = i2l(i, j, list.get(k), b)[0];
		}
		
		return clause;
	}
	
	/**
	 * Return !(x_(i1, j1, c) | x_(i2, j2, c)) clauses
	 * 
	 * @param i1 index number
	 * @param j1 index number
	 * @param c the number between 1 and 9 
	 * @param i2 index number
	 * @param j2 index number
	 * @return !(x_(i1, j1, c) | x_(i2, j2, c)) clauses
	 */
	private VecInt oppClause(int i1, int j1, int c, int i2, int j2) {
		int[] clause = new int[2];
		clause[0] = i2l(i1, j1, c, false)[0];
		clause[1] = i2l(i2, j2, c, false)[0];
		return new VecInt(clause);
	}
	
	/**
	 * Returns numbers already appeared in either the box or the lines
	 * 
	 * @param type either "Box", "Horizontal", or "Vertical"
	 * @param i index number
	 * @param j index number
	 * @return the numbers already appeared in either the box or the lines
	 */
	private List<Integer> getNegNums(String type, int i, int j) {
		List<Integer> fixedNums = new ArrayList<Integer>();
		if(type == "Box") {
			//Let i and j be either 1, 4, or 7
			i = ((i-1) / 3) * 3 + 1;
			j = ((j-1) / 3) * 3 + 1;
			int I = i + 3;
			int J = j + 3;
			int jCopy = j;
			for (;i < I; i++) {
				for(; j < J ; j++) {
					if(this.cell[i][j] != -1) {
						fixedNums.add(this.cell[i][j]);
					}
				}
				j = jCopy;
			}
		}
		//Horizontal search: i is fixed and j varies
		else if(type == "Horizontal") {
			for(j = 1; j <= 9 ; j++) {
				if(this.cell[i][j] != -1) {
					fixedNums.add(this.cell[i][j]);
				}
			}
		}
		//Vertical search: i varies and j is fixed
		else {
			for(i = 1; i <= 9 ; i++) {
				if(this.cell[i][j] != -1) {
					fixedNums.add(this.cell[i][j]);
				}
			}
		}
		return fixedNums;
	}
	
	/**
	 * Add positive and negative clause of a cell(i, j) with the given negative numbers
	 * 
	 * @param i index number
	 * @param j index number
	 * @param The numbers already appeared either in the box or the lines
	 */
	private void addGivenClauses(int i, int j, Set<Integer> negNums) {
		int k;
		Set<Integer> posNums = new HashSet<Integer>();
		List<Integer> negList = new ArrayList<Integer>();
		
		//If this is an even cell --> only even numbers but fixed numbers allowed
		if(isEven[i][j]) {  
			posNums.addAll(evenNums);
			posNums.removeAll(negNums);
			negNums.addAll(oddNums);
		}
		//If this is an odd cell --> only odd numbers but fixed numbers allowed
		else {
			posNums.addAll(oddNums);
			posNums.removeAll(negNums);
			negNums.addAll(evenNums);
		}
		try {
			this.solver.addClause(new VecInt(list2clause(i, j, new ArrayList<Integer>(posNums), true)));
			negList.addAll(negNums);
			for(k = 0 ; k < negNums.size(); k++) {
				this.solver.addClause(new VecInt(i2l(i, j, negList.get(k), false)));
			}
		} catch (ContradictionException e) {
			this.solvable = false;
		}
		posNums.clear();
		negList.clear();
	}
	
	/**
	 * Add three rule for a cell
	 * 1. the box rule : There should be no duplicated numbers in a box
	 * 2. the horizontal rule : There should be no duplicated numbers in a horizontal line
	 * 3. the vertical rule : There should be no duplicated numbers in a vertical line
	 * 
	 * @param i cell index
	 * @param j cell index 
	 */
	private void addUngivenClauses(final int i, final int j) {
		List<Integer> candidBox = new ArrayList<Integer>(isEven[i][j] ? evenNums : oddNums);
		List<Integer> candidHor = new ArrayList<Integer>(isEven[i][j] ? evenNums : oddNums);
		List<Integer> candidVer = new ArrayList<Integer>(isEven[i][j] ? evenNums : oddNums);
		
		try {
			//For the box rule : There should be no duplicated numbers in a box
			int box_i = ((i-1) / 3) * 3 + 1;
			int box_j = ((j-1) / 3) * 3 + 1;
			int I = box_i + 3;
			int J = box_j + 3;
			candidBox.removeAll(getNegNums("Box", i, j));
			for(;box_i < I ; box_i++) {
				for(;box_j < J ; box_j++) {
					if(!((i == box_i) && (j == box_j)) && cell[box_i][box_j] == -1
							&& (isEven[i][j] == isEven[box_i][box_j])) {
						for(int c: candidBox) {
							solver.addClause(oppClause(i, j, c, box_i, box_j));
						}
					}
				}
				box_j -= 3;
			}
			//For the horizontal rule : There should be no duplicated numbers in a horizontal line
			candidHor.removeAll(getNegNums("Horizontal", i, 0));
			for(int k = 1 ; k <= 9 ; k++) {
				if(j != k && cell[i][k] == -1 && (isEven[i][j] == isEven[i][k])) {
					for(int c: candidHor) {
						solver.addClause(oppClause(i, j, c, i, k));
					}
				}
			}
			//For the vertical rule : There should be no duplicated numbers in a vertical line
			candidVer.removeAll(getNegNums("Vertical", 0, j));
			for(int k = 1 ; k <= 9 ; k++) {
				if(i != k && cell[k][j] == -1 && (isEven[i][j] == isEven[k][j])) {
					for(int c: candidVer) {
						solver.addClause(oppClause(i, j, c, k, j));
					}
				}
			}
		} catch (ContradictionException e) {
		}
		candidBox.clear();
		candidHor.clear();
		candidVer.clear();
	}

	/**
	 * Returns a set of Solution instance for the given sudoku puzzle.
	 * 
	 * @return the set of solutions
	 */
	public Set<Solution> solve(){
		//TODO implement this
		int i, j;
		
		Set<Integer> negNums = new HashSet<Integer>();
		for(i = 1 ; i <= 9 ; i++) {
			for(j = 1 ; j <= 9 ; j++) {
				if(cell[i][j] == -1) {
					negNums.addAll(getNegNums("Box", i, j));
					negNums.addAll(getNegNums("Horizontal", i, 0));
					negNums.addAll(getNegNums("Vertical", 0, j));
					addGivenClauses(i, j, negNums);
					addUngivenClauses(i, j);
					negNums.clear();		
				}
			}
		}
		if(this.solvable)
			return var2sols();
		else
			return null;
	}
	/**
	 * Convert 9*9*9 variables to 9*9 cell solution(s)
	 * Since solutions could be more than one, this should find solutions recursively
	 * 
	 * @return Solution set
	 */	
	private Set<Solution> var2sols() {
		Set<Solution> solSet = new HashSet<Solution>();
		try {
			while(this.solver.isSatisfiable()) {
				Solution sol = new Solution(this);

				//Assign the SAT result
				for(int i = 1 ; i <= 9 ; i++) {
					for(int j = 1 ; j <= 9 ; j++) {
						for(int k = 1 ; k <= 9 ; k++) {
							if(solver.model(100*i + 10*j + k)) {
								sol.setValue(i, j, k);
							}
						}
					}
				}
				//For debug purpose
				//sol.print();
				solSet.add(sol);	
				int[] blockingClause = getBlockingClause(solver.model());
				solver.addBlockingClause(new VecInt(blockingClause));
			}
		} catch (ContradictionException e) {
		} catch (TimeoutException e) {
			System.out.println("Timeout Exception");
		}
		return solSet;
		//For debug purpose
//		for(int i = 1 ; i <= 9 ; i++) {
//			for(int j = 1; j <= 9 ; j++) {
//				System.out.format("%d ", sol.getValue(i, j));
//			}
//			System.out.println("");
//		}
		
	}
	/**
	 * Returns the corresponding blocking clause
	 * 
	 * @param clause the clause
	 * @return every element is negated from the argument clause
	 */
	private int[] getBlockingClause(int[] clause) {
		int[] blockingClause = new int[clause.length];
		for(int i= 0 ; i < clause.length; i++)
			blockingClause[i] = -1 * clause[i];
		return blockingClause;
	}

	/**
	 * Returns true if a given cell must contain an even number.
	 * 
	 * @param row row number
	 * @param column column number 
	 * @return true if a cell (row, column) must be filled by an even number; otherwise, return false.
	 */
	public Boolean isEven(int row, int column) {
		//TODO implement this
		if(1 <= row && row <= 9 && 1 <= column && column <= 9)
			return isEven[row][column];
		else 
			return null;
	}
	
	/**
	 * Return the initial value of a cell, given an even/odd Sudoku puzzle.
	 * If there is no value to get (e.g. out of range, or empty), return null.
	 * 
	 * @param row row number
	 * @param column column number
	 * @return A value of a cell, if it is non-empty
	 */
	public Integer getValue(int row, int column) {
		//TODO implement this
		if(1 <= row && row <= 9 && 1 <= column && column <= 9 && cell[row][column] != -1)
			return cell[row][column];
		else
			return null;
	}
}