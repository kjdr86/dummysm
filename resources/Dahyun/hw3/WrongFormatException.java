package edu.postech.csed332.homework3;

public class WrongFormatException extends Exception {
    
    private int Err_Code;
     
    public WrongFormatException (String msg, int errCode) {
        super(msg);
        this.Err_Code = errCode;
    }
     
    public WrongFormatException (String msg){
        this(msg, 100);
    }
     
    public WrongFormatException() {
		// TODO Auto-generated constructor stub
	}

	public int getErrCode(){
        return Err_Code;
    }
}