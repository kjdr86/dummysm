package edu.postech.csed332.homework3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Receives a sudoku puzzle as a file by the command line argument,
 * solves the puzzle, and makes a file for each solution
 * 
 * For example, if you received an input sudoku file "A" and the 
 * solver gives 3 solutions for the puzzle, the output files 
 * "A_1.solution", "A_2.solution" and "A_3.solution" will be made.
 *
 */

public class Game {
	private Sudoku sudoku;
	private Set<Solution> solution;
	private String fileName;
	private String fileExtension;
	
	/**
	 * Read sudoku text file and parse it as a string
	 * 
	 * @param fileName the given file name
	 * @return a parsed string
	 */	
	public List<String> readSudoku(String fileName) throws FileNotFoundException{
		List<String> str = new ArrayList<String>();
		try {
	    	//Open
	    	File file = new File(fileName);
	    	if(!file.exists()) {
	    		throw new FileNotFoundException();	    		
	    	}

	    	FileReader filereader = new FileReader(file);
	    	BufferedReader bufreader = new BufferedReader(filereader);
	    	//Read
	    	String s;
	    	
	    	while ((s = bufreader.readLine()) != null ) {
	    		str.add(s);
	    	}
	    	
	    	//Close
	    	bufreader.close();
	    	filereader.close();

	    }
	    catch (IOException e) {
	    	throw new FileNotFoundException();	
	    }
	    
	    return str;
	}
	
	/**
	 * Parse the string and store the value to the new sudoku class
	 * @param fileName the given file name
	 */
	
	Game(String fileName) throws FileNotFoundException, WrongFormatException {
		List<String> sudokuStr = null;
		try {
			sudokuStr = this.readSudoku(fileName);
		} catch (FileNotFoundException e) {
			throw e;
		}

		this.fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1);
		if(this.fileExtension.equals("txt") || this.fileExtension.equals("cat")) {
			this.fileName = fileName.substring(0, fileName.lastIndexOf("."));
		}
		else
			this.fileName = fileName;
		
		try {
			this.sudoku = new Sudoku(sudokuStr);
		} catch (WrongFormatException e) {
			throw e;
		}
		this.solution = new HashSet<Solution>();
	}

	/**
	 * Write text file(s) of solutions
	 * Reference : https://www.geeksforgeeks.org/delete-file-using-java/
	 */
	public void writeSolutions() {
		int solNum = 1;
		
		for(Solution s : this.solution) {
			File fout = new File(this.fileName+"_"+Integer.toString(solNum)+".solution");
			FileOutputStream fos;
			try {
				fos = new FileOutputStream(fout);
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
				
				for(int i = 1 ; i <= 9 ; i++) {
					for(int j = 1 ; j <= 9 ; j++) {
						bw.write(Integer.toString(s.getValue(i, j)));
					}
					bw.newLine();
				}
				solNum++;
				
				bw.close();
				fos.close();
			} catch (IOException e) {
				System.out.println("IO Exception in writeSolutions()");
			}
		}
	}
	
	/**
	 * Get solution file name by the rule
	 * 
	 * @param fileName the file name
	 * @param i the solution number
	 */
	private static String getSolFileName(String fileName, int i) {
		final String dir = System.getProperty("user.dir");
		return dir+"\\"+fileName + "_" + Integer.toString(i) + ".solution";
	}
	/**
	 * Delete all output files
	 * 
	 * @param args input arguments
	 */
	public static void deleteAllOutFiles(String[] args) {
		if(args.length == 1)
			return ;
		
		String fileName = null;
		String fileExtension = null;
		int i = 1;
		
		fileExtension = args[1].substring(args[1].lastIndexOf(".") + 1);
		if(fileExtension.equals("txt") || fileExtension.equals("cat")) {
			fileName = args[1].substring(0, args[1].lastIndexOf("."));
		}
		else
			fileName = args[1];
		
		for(Path deleteFilePath = Paths.get(getSolFileName(fileName, i)); 
				Files.exists(deleteFilePath); 
				deleteFilePath = Paths.get(getSolFileName(fileName, ++i)))
		{
					try {
						Files.deleteIfExists(deleteFilePath);
					} catch (IOException e) { //No worries to get caught
					} 
		}
	}
	
	/**
	 * Verify result files
	 * 
	 * @param args input arguments
	 */
	public static boolean verifyOutFiles(String[] args) {
		if(args.length == 1)
			return true;
		
		String fileName = null;
		String fileExtension = null;
		int i = 1;
		
		//Get file name
		fileExtension = args[1].substring(args[1].lastIndexOf(".") + 1);
		if(fileExtension.equals("txt")|| fileExtension.equals("cat")) {
			fileName = args[1].substring(0, args[1].lastIndexOf("."));
		}
		else
			fileName = args[1];
		
		for(String outFileAbsName = getSolFileName(fileName, i); 
				Files.exists(Paths.get(outFileAbsName)); 
				outFileAbsName = getSolFileName(fileName, ++i))
		{
	        Solution solved = null;
			try {
				solved = new Solution(outFileAbsName);
			} catch (FileNotFoundException e) { //No worries to get caught
			}
			if(!solved.verify()) {
				return false; 
			}
		}
		return true;
	}
	
	/**
	 * @param args name of the input file for a sudoku puzzle
	 * 			args[0] is the name of this program
	 * 			args[1] is the name of the input file.
	 */
	public static void main(String[] args) {
		System.out.println("");
		System.out.println("====================Sudoku solver=====================");
		System.out.println("Tip: only *.txt and *.cat are allowed as an input file.");

		if(args.length == 1) {
			System.out.println("================ERROR: No file input.=================");
			return ;
		}
		Game g = null;
		try {
			g = new Game(args[1]);
		} catch (FileNotFoundException e) {
			System.out.println("=========ERROR: The file has not been found.==========");
			return ;
		} catch (WrongFormatException e) {
			System.out.println("=======ERROR: " + e.getMessage()+"=======");
			return ;
		}
		
		g.solution = g.sudoku.solve();
		if(g.solution == null ) {
			System.out.println("============ERROR: No solution available.=============");
			return ;
		}
		
		if(g.solution.size() == 0){
			System.out.println("============ERROR: No solution available.=============");
			return ;
		}
		
		System.out.println("==========All output solution(s) is correct===========");
		g.writeSolutions();		
	}
}
