package edu.postech.csed332.homework3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * An instance of this class represents a single solution for a sudoku puzzle
 * 
 * You can freely add member variables and methods to implement this class
 * 
 */
public class Solution {
	//TODO implement this
	private int cell[][];
	
	Solution (Sudoku s) {
		this.cell = new int[10][10];
		
		for(int i= 1 ; i <= 9 ; i++) {
			for(int j= 1 ; j<= 9 ; j++) {
				this.cell[i][j] = s.getValue(i, j) == null ? -1 : s.getValue(i, j);
			}
		}
	}
	Solution (String fileName) throws FileNotFoundException {
		List<String> str = new ArrayList<String>();
		try {
	    	//Open
	    	File file = new File(fileName);
	    	if(!file.exists()) {
	    		throw new FileNotFoundException();	    		
	    	}

	    	FileReader filereader = new FileReader(file);
	    	BufferedReader bufreader = new BufferedReader(filereader);
	    	//Read
	    	String s;
	    	
	    	while ((s = bufreader.readLine()) != null ) {
	    		str.add(s);
	    	}
	    	
	    	//Close
	    	bufreader.close();
	    	filereader.close();

	    }
	    catch (IOException e) {
	    	throw new FileNotFoundException();	  
	    }
	    
		this.cell = new int[10][10];
		for(int i = 1 ; i <= 9 ; i++) {
			for(int j = 1 ; j <= 9 ; j++) {
				this.cell[i][j] = Character.getNumericValue(str.get(i - 1).charAt(j - 1));
			}	
		}
	}
	/**
	 * Returns a value of a cell in the solution. If the input is out of range, return null.
	 * 
	 * For example, in the example puzzle in the homework description, cell (1,3) will be 
	 * filled by number 8 by the solution. In this case, getValue(1,3) should return 8.
	 * 
	 * @param row row number
	 * @param column column number
	 * @return Value of a (row, column) cell, provided by a given solution
	 */	
	public Integer getValue(int row, int column) {
		//TODO implement this
		if(1 <= row && row <= 9 && 1 <= column && column <= 9)
			return cell[row][column];
		else
			return null;
	}

	
	/**
	 * Set the value of the cell as the given number
	 * 
	 * @param i row number
	 * @param j column number
	 * @param the given number
	 */
	public boolean setValue(final int row, final int column, final int n) {
		if(1 <= row && row <= 9 && 1 <= column && column <= 9) {
			this.cell[row][column] = n;
			return true;
		}
		else
			return false;
	}
	/**
	 * Verify the solution is correct based on the sudoku rule
	 * Reference : https://codereview.stackexchange.com/questions/46033/sudoku-checker-in-java/46037
	 * 
	 * @return True if the sudoku solution is correct
	 */
	public boolean verify() {
		List<Integer> row = new ArrayList<Integer>(9);
		List<Integer> col = new ArrayList<Integer>(9);
		List<Integer> box = new ArrayList<Integer>(9);
	    for (int i = 1; i <= 9; i++) {
	        for (int j = 1; j <= 9; j ++) {
	        	//For debug purpose
	        	//System.out.format("%d %d\n", ((i-1)/3)*3 + (j-1)/3 + 1, ((i-1)%3)*3 + j -(((j-1) /3) * 3));
	            box.add(cell[((i-1)/3)*3 + (j-1)/3 + 1][((i-1)%3)*3 + j -(((j-1) /3) * 3)]);
	        	row.add(cell[i][j]);
	            col.add(cell[j][i]);
	        }
	        if (!(is9nums(box) && is9nums(row) && is9nums(col)))
	            return false;
	        row.clear();
	        col.clear();
	        box.clear();
	    }
	    return true;
	}
	
	/**
	 * Verify the 9 numbers have no duplicated numbers
	 * Reference : https://codereview.stackexchange.com/questions/46033/sudoku-checker-in-java/46037
	 * 
	 * @param 9 numbers from either a row, a column, or a box
	 * @return True if arr contains 1 to 9
	 */
	private boolean is9nums(List<Integer> arr) {
	    int i = 0;
	    Collections.sort(arr);
	    for (int num : arr) {
	        if (num != ++i) {
	            return false;
	        }
	    }
	    return true;
	}
	public void print() {
		String s = "";
		for(int i = 1 ; i <= 9 ; i ++) {
			for(int j = 1 ; j <= 9 ; j++) {
				s += Integer.toString(cell[i][j]);
			}
			System.out.println(s);
			s = "";
		}
		System.out.println("");
	}
}
