package edu.postech.csed332.homework3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.TimeoutException;

/**
 * 
 * An instance of this class represents a even/odd Sudoku puzzle. This class must be 
 * immutable. That is, you should define a constructor to create a Sudoku object with 
 * all the necessary information for a concrete even/odd Sudoku puzzle.
 * 
 * You can freely add member variables and methods to implement this class.
 * 
 */
public class Sudoku {
	private int[][] data;
	private int[][][] deformed;
	
	
	//TODO implement this
	
	
	Sudoku(int[][] data)
	{
		this.deformed = new int[9][9][2];
		this.data = new int[9][9];
		
		for(int i = 0 ; i < 9 ; i++)
		{
			for(int j = 0 ; j< 9 ; j++)
			{
				this.data[i][j] = data[i][j];
			}
		}
	}
	
	
	
	
	/**
	 * Returns a set of Solution instance for the given sudoku puzzle.
	 * 
	 * @return the set of solutions
	 */
	public Set<Solution> solve(){
		//I have to count up the number of clauses.
		//the number of clauses don't exceed 4C2*4*9*3 + 5C2*5*9*3 + 81 = 4077 - new int[4077][]..?
		//let's use ArrayList.
		List<int[]> clauses = new ArrayList<int[]>();
		
		/****************************************************************************************************/
		//first, already solved number will be encoded as single variable clause.
		for(int i = 0 ; i < 9 ; i++)
		{
			for(int j = 0 ; j < 9 ; j++)
			{
				int num = data[i][j];
				if(num >= 1 && num <=9)
				{
					int[] known = new int[1];
					known[0] = cal(i,j,num);	//it should be true!
					
					clauses.add(known);
				}
			}
		}
		//already known variables - Done - Correct
		//for each cell, it should be compared to all of cells which are in same row, column, or 3*3 box.
		/****************************************************************************************************/
		
		/****************************************************************************************************/
		//second, let's consider each row, along it is even or odd.
		for(int r = 0 ; r < 9 ; r++)
		{
			for(int i = 0 ; i < 8 ; i++)
			{
				if( isEven(r+1,i+1) )
					continue;
						
				for(int j = i+1 ; j<9 ; j++)
				{
					if( isEven(r+1,j+1) )
						continue;
					
					//(r,i) and (r,j) are both odd.
					//they should be different!
					for(int odd = 1 ; odd <= 9 ; odd = odd+2)
					{
						int[] unknownOdd = new int[2];
						unknownOdd[0] = -cal(r,i,odd);
						unknownOdd[1] = -cal(r,j,odd);	//encoding
						
						clauses.add(unknownOdd);
					}
				}
			}	// Odd number clauses done.
			
			for(int i = 0 ; i < 8 ; i++)
			{
				if( !isEven(r+1,i+1) )
					continue;
				
				for(int j = i+1; j < 9 ; j++)
				{
					if( !isEven(r+1, j+1) )
						continue;
					
					for(int even = 2 ; even <= 8 ; even = even+2)
					{
						int[] unknownEven = new int[2];
						unknownEven[0] = -cal(r,i,even);
						unknownEven[1] = -cal(r,j,even);
						
						clauses.add(unknownEven);
					}
				}
			}	// Even number clauses done.
		}
		// Row clauses done.
		/****************************************************************************************************/
		
		/****************************************************************************************************/
		//third, let's consider each column.
		for(int c=0 ; c<9 ; c++)
		{
			for(int i=0 ; i<8 ; i++)
			{
				if( isEven(i+1, c+1) )
					continue;
				
				for(int j = i+1 ; j < 9 ; j++)
				{
					if( isEven(j+1, c+1) )
						continue;
					
					for(int odd=1 ; odd<=9 ; odd = odd+2)
					{
						int[] unknownOdd = new int[2];
						unknownOdd[0] = -cal(i,c,odd);
						unknownOdd[1] = -cal(j,c,odd);
						
						clauses.add(unknownOdd);
					}
				}
			}	//Odd number clauses done.
			
			for(int i = 0 ; i<8 ; i++)
			{
				if( !isEven(i+1, c+1) )
					continue;
				
				for(int j=i+1 ; j<9 ; j++)
				{
					if( !isEven(j+1, c+1) )
						continue;
					
					for(int even = 2 ; even <= 8 ; even = even + 2)
					{
						int[] unknownEven = new int [2];
						unknownEven[0] = -cal(i,c,even);
						unknownEven[1] = -cal(j,c,even);
						
						clauses.add(unknownEven);
					}
				}
			}	//Even number clauses done.
		}
		// Column clauses done.
		/****************************************************************************************************/
		
		/****************************************************************************************************/
		// Lastly, I have to consider 3*3 grid box.
		// let's make deformed array to compare easily.
		
		int blockCount=0;
		for(int rb = 0 ; rb < 9 ; rb = rb + 3)
		{
			for(int cb = 0 ; cb < 9 ; cb = cb + 3)
			{
				int col=0;
				
				for(int ri = rb ; ri < rb+3 ; ri++)
				{
					for(int ci = cb ; ci < cb+3 ; ci++)
					{
						deformed[blockCount][col][0] = data[ri][ci];
						deformed[blockCount][col][1] = cal(ri,ci,0);	//tricky
						
						col++;
					}
				}
				blockCount++;
			}
		}
		//deforming done. - Correct
	
		
		
		for(int r = 0 ; r < 9 ; r++)
		{
			for(int i = 0 ; i < 8 ; i++)
			{
				if( isEvenD(r+1,i+1) )
					continue;
				
				for(int j = i + 1 ; j< 9 ; j++)
				{
					if( isEvenD(r+1,j+1) )
						continue;
					
					for(int odd = 1 ; odd <= 9 ; odd = odd+2)
					{
						int[] unknownOdd = new int[2];
						unknownOdd[0] = -(deformed[r][i][1] + odd);
						unknownOdd[1] = -(deformed[r][j][1] + odd);
						
						clauses.add(unknownOdd);
					}
				}
			}	// Odd number done.
			
			for(int i = 0 ; i < 8 ; i++)
			{
				if( !isEvenD(r+1,i+1) )
					continue;
				
				for(int j = i+1; j < 9 ; j++)
				{
					if( !isEvenD(r+1, j+1) )
						continue;
					
					for(int even = 2 ; even <= 8 ; even = even+2)
					{
						int[] unknownEven = new int[2];
						unknownEven[0] = -(deformed[r][i][1] + even);
						unknownEven[1] = -(deformed[r][j][1] + even);
						
						clauses.add(unknownEven);
					}
				}
			}	// Even number done.
		}
		// 3*3 grid done.
		/****************************************************************************************************/
		
		
		/****************************************************************************************************/
		// one more, each cell's information should be updated.
		for(int i = 0 ; i < 9 ;  i++)
		{
			for(int j = 0 ; j < 9 ; j++)
			{
				if( isEven(i+1,j+1) )
				{
					int[] unknownEven = new int[4];
					
					for(int even = 2 ; even <= 8 ; even = even + 2)
					{
						unknownEven[even/2 - 1] = cal(i,j,even);
					}
					
					clauses.add(unknownEven);
				}
				else
				{
					int[] unknownOdd = new int[5];
					for(int odd = 1 ; odd <= 9 ; odd = odd + 2)
					{
						unknownOdd[odd/2] = cal(i,j,odd);
					}

					clauses.add(unknownOdd);
				}
			}
		}
		/****************************************************************************************************/
		// Done.
		
		/*All clauses are calculated*/
		Set<Solution> solutions = new HashSet<Solution>();
		SATChecker checker;
		boolean[] result;
		Solution s;
		
		for(;;)
		{
			checker= new SATChecker(729, clauses.size());
			result = new boolean[] {};
			try
			{
				result = checker.solve( clauses.toArray(new int[0][0]) );
				
			} catch (ContradictionException e) {
			} catch (TimeoutException e) {
			}
			
			s = new Solution(result);
			solutions.add(s);
			
			if(checker.hasOnlySolution == true)
			{
				return solutions;
			}
			else
			{
				int[] duplicate = new int[81];
				int counter = 0;
				
				for(int i = 1 ; i < 10 ; i++)
				{
					for(int j = 1 ; j < 10 ; j++)
					{
						duplicate[counter] = -cal(i-1,j-1, s.getValue(i, j));
						counter++;
					}
				}
				
				clauses.add(duplicate);
			}
		}
		
		//TODO implement this
	}
	
	public int cal(int row, int col, int num)
	{
		return row * 81 + col * 9 + num;	//unique number for each variable.
	}
	
	
	public Boolean isEvenD(int row, int column) {
		if(deformed[row-1][column-1][0] == -6)
			return true;	// * : -6
		else if (deformed[row-1][column-1][0] == -2)
			return false;	// . : -2
		else
		{
			if(deformed[row-1][column-1][0] % 2 == 0)
				return true;
			else
				return false;
		}
		//TODO implement this - Done
	}
	
	
	/**
	 * Returns true if a given cell must contain an even number.
	 * 
	 * @param row row number
	 * @param column column number 
	 * @return true if a cell (row, column) must be filled by an even number; otherwise, return false.
	 */
	public Boolean isEven(int row, int column) {
		if(data[row-1][column-1] == -6)
			return true;	// * : -6
		else if (data[row-1][column-1] == -2)
			return false;	// . : -2-
		else
		{
			if(data[row-1][column-1] % 2 == 0)
				return true;
			else
				return false;
		}
		//TODO implement this - Done
	}
	
	/**
	 * Return the initial value of a cell, given an even/odd Sudoku puzzle.
	 * If there is no value to get (e.g. out of range, or empty), return null.
	 * 
	 * @param row row number
	 * @param column column number
	 * @return A value of a cell, if it is non-empty
	 */
	public Integer getValue(int row, int column) {
		int target = data[row-1][column-1];
		
		if(target == -2)
			return null;
		else if(target == -6)
			return null;
		
		if(row < 1 || row > 9)
			return null;
		else if(column < 1 || column > 9)
			return null;
		
		return target;
		
		//TODO implement this - Done
	}
	
}