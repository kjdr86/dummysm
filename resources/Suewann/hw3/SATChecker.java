package edu.postech.csed332.homework3;

import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;
import org.sat4j.tools.SingleSolutionDetector;


public class SATChecker {
	private ISolver solver;
	private int numVar;
	public boolean hasOnlySolution;

	/**
	 * @param newVar number of variables in the formula
	 * @param expectNumClause number of clauses in the formula
	 */
	SATChecker(int newVar, int expectNumClause) {
		this.numVar = newVar;
		this.solver = SolverFactory.newDefault();
		this.solver.newVar(newVar);
		this.solver.setExpectedNumberOfClauses(expectNumClause);
	}

	/**
	 * SATChecker adds each clause and make it into a CNF formula.
	 * 
	 * @param clauses array of clauses in a CNF formula
	 * @return The assignment of the CNF formula only if the formula is satisfiable. Returns null otherwise.
	 * @throws ContradictionException
	 * @throws TimeoutException
	 */
	public boolean[] solve(int[][] clauses) throws ContradictionException, TimeoutException {
		for (int i = 0; i < clauses.length; ++i) {
			this.solver.addClause(new VecInt(clauses[i]));
		}

		if (this.solver.isSatisfiable()) {
			boolean [] result = new boolean[numVar];
			for(int i=0; i < numVar; i++) {
				result[i]=solver.model(i+1);
			}
			
			SingleSolutionDetector problem = new SingleSolutionDetector(solver);
			this.hasOnlySolution = problem.hasASingleSolution();
			
			
			return result;
		}

		return null;
	}

}
