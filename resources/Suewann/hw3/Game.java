package edu.postech.csed332.homework3;

import java.io.*;
import java.util.Set;


/**
 * Receives a sudoku puzzle as a file by the command line argument,
 * solves the puzzle, and makes a file for each solution
 * 
 * For example, if you received an input sudoku file "A" and the 
 * solver gives 3 solutions for the puzzle, the output files 
 * "A_1.solution", "A_2.solution" and "A_3.solution" will be made.
 *
 */
public class Game {
	private static Sudoku sudoku;
	//TODO implement this - Done
	
	/**
	 * @param args name of the input file for a sudoku puzzle
	 */
	public static void main(String[] args) throws IOException, FileNotFoundException {
		int[][] data = new int[9][9];

		// data reading start.
		File infile = new File(args[0]);
		FileReader filereader = new FileReader(infile);
		BufferedReader bufReader = new BufferedReader(filereader);
		 int lineCount = 0;
		 String line = "";
		 
		 System.out.println(System.getProperty("user.dir"));
		 
		while( (line = bufReader.readLine()) != null )
		{
			for(int i = 0 ; i < 9 ; i++)
			{
				data[lineCount][i] = ((int) line.charAt(i)) - 48; 	// read as ASCII code
			}														// * is saved as -6, . is saved as -2
			lineCount++;
		}
		bufReader.close();
		// data reading done.
		
		
		sudoku = new Sudoku(data);	//TODO - Done
		
		
		// result writing start.
		int solutionCount = 1;
		Set<Solution> ss = sudoku.solve();
		
		for(Solution s : ss)
		{
			String[] temp = args[0].split("\\.");
			String fileName = temp[0] + "_" + String.valueOf(solutionCount) + ".solution";
			//delete ".txt" and add "_i.solution
			
			File outfile = new File(fileName);
			FileWriter filewriter = new FileWriter(outfile);
			BufferedWriter bufferedWriter = new BufferedWriter(filewriter);
			
			for(int i = 1 ; i < 10 ; i++)
			{
				for(int j = 1 ; j < 10 ; j++)
				{
					char tempchar = (char) (s.getValue(i, j) + 48);
					bufferedWriter.append( tempchar ); // write as ASCII code with casting
				}
				
				if(i < 9)
				{
					bufferedWriter.newLine();
				}
			}
			bufferedWriter.close();
			solutionCount++;
		}
		// result writing done.
		
		
		//TODO implement this - Done
	}

}
