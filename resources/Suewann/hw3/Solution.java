package edu.postech.csed332.homework3;

/**
 * 
 * An instance of this class represents a single solution for a sudoku puzzle
 * 
 * You can freely add member variables and methods to implement this class
 * 
 */
public class Solution {
	private int[][] answer;
	//TODO implement this
	
	Solution(boolean[] result)
	{
		answer = new int[9][9];
		
		
		for(int i = 0 ; i < 9 ; i++)
		{
			for(int j = 0 ; j < 9 ; j++)
			{
				int indexing = i*81 + j*9;
				for(int target = indexing ; target < indexing + 9 ; target++)
				{
					if(result[target] == true)
					{
						answer[i][j] = target - indexing + 1;
						break;
					}
					else
					{
						continue;
					}
				}
			}
		}
	}
	
	/**
	 * Returns a value of a cell in the solution. If the input is out of range, return null.
	 * 
	 * For example, in the example puzzle in the homework description, cell (1,3) will be 
	 * filled by number 8 by the solution. In this case, getValue(1,3) should return 8.
	 * 
	 * @param row row number
	 * @param column column number
	 * @return Value of a (row, column) cell, provided by a given solution
	 */
	public Integer getValue(int row, int column) {
		if(row < 1 || row > 9)
			return null;
		else if(column < 1 || column > 9)
			return null;
		
		int target = answer[row-1][column-1];
		return target;
		//TODO implement this - Done
	}
}
