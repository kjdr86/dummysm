package edu.postech.csed332.homework1;

/**
 * The interest of HighInterest account is 1% per day. For example, one person puts 100
 * dollars in his HighInterest Account(At Day 1). After 10 days(Day 11), 
 * the balance of the account will be 100*(1.01)^10.
 * The balance of HighInterest account should always bigger or equal to 1000. 
 */
class HighInterest implements Account {
	double AccountNumber;
	double Balance;
	String Owner;
	ACCTYPE type;
	
	public ACCTYPE getACCTYPE() { return type;}
	
	public double getAccountNumber() { return AccountNumber;}
	public double getBalance() { return Balance;}
	public String getOwner() { return Owner;}
	
	public void updateBalance(int elapsedDate)
	{
		Balance = Balance * (Math.pow(1010, elapsedDate));
		Balance = Balance*(Math.pow(0.001, elapsedDate));
	}
	public void deposit(double amount)
	{
		Balance = Balance + amount;
	}
	public void withdraw(double amount) throws NotEnoughException
	{
		if(Balance - amount < 1000)
		{
			throw new NotEnoughException();
		}
		else
		{
			Balance = Balance - amount;
		}
	}
	
	HighInterest(String name, double initial) throws NotEnoughException
	{
		Balance = initial;
		Owner = name;
		type = ACCTYPE.HIGH;
		
		if(initial < 1000)
		{
			throw new NotEnoughException();
		}
			
	}
	
//TODO implement this - Done
}