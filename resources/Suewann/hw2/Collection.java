package edu.postech.csed332.homework2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * The Collection class represents a single collection. It contains a name of
 * the collection and also has a list of references to every book and every
 * sub-collection in that particular collection.
 * 
 * A collection can be exported to and imported from a string representation.
 * The format of the string is of your choice, but the string representation
 * should have the name of this collection and all elements (books and
 * sub-collections) contained in this collection.
 */
public final class Collection extends Element {
	List<Element> elements;
	private String name;

	/**
	 * Creates a new collection with the given name.
	 *
	 * @param name the name of the collection
	 */
	public Collection(String name) {
		this.name = name;
		elements = new ArrayList<Element>();
		// TODO implement this
	}

	/**
	 * Restores a collection from its given string representation.
	 *
	 * @param stringRepresentation the string representation
	 */
	public static Collection restoreCollection(String stringRepresentation) {
		Collection result;
		
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(stringRepresentation);
		
		result = new Collection(element.getAsJsonObject().get("name").getAsString());
		result.elements.clear();
		
		JsonObject arr = element.getAsJsonObject().get("elements").getAsJsonObject();
		
		Set<Map.Entry<String, JsonElement>> entries = arr.entrySet();
		for(Map.Entry<String, JsonElement> entry : entries)
		{
			if(entry.getKey().charAt(0) == 'b')
			{
				Book newBook = new Book(entry.getValue().getAsString());
				result.addElement(newBook);
			}
			else if(entry.getKey().charAt(0) == 'c')
			{
				Collection newCollection = restoreCollection(entry.getValue().getAsString());
				result.addElement(newCollection);
			}
		}
		
		return result;
		// TODO implement this - maybe done
	}

	/**
	 * Returns the string representation of a collection. The string representation
	 * should have the name of this collection, and all elements
	 * (books/subcollections) contained in this collection.
	 *
	 * @return string representation of this collection
	 */
	public String getStringRepresentation() {
		Gson gson = new Gson();
		
		JsonObject collection = new JsonObject();
		collection.addProperty("name", this.name);
		
		JsonObject elementsAsString = new JsonObject();
		
		int i = 1;
		for(Element elem : elements)
		{
			if(elem instanceof Book)
			{
				elementsAsString.addProperty("b" + String.valueOf(i), ((Book)elem).getStringRepresentation());
			}
			else
			{
				elementsAsString.addProperty("c" + String.valueOf(i), ((Collection)elem).getStringRepresentation());
			}
			i++;
		}
		
		JsonElement element = gson.fromJson(elementsAsString.toString(), JsonElement.class);
		collection.add("elements", element);
		
		String json = gson.toJson(collection);
		
		return json;
		// TODO implement this - Done..?
	}

	/**
	 * Returns the name of the collection.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
		// TODO implement this - Done
	}

	/**
	 * Adds an element to the collection. If parentCollection of given element is
	 * not null, do not actually add, but just return false. (explanation: if given
	 * element is already a part of another collection, you should have deleted the
	 * element from that collection before adding to another collection)
	 *
	 * @param element the element to add
	 * @return true on success, false on fail
	 */
	public boolean addElement(Element element) {
		if(element.getParentCollection() != null)
			return false;
		else
		{
			element.setParentCollection(this);			
			elements.add(element);
			
			return true;
		}
		// TODO implement this - Done
	}

	/**
	 * Deletes an element from the collection. Returns false if the collection does
	 * not have this element. Hint: Do not forget to clear parentCollection of given
	 * element
	 *
	 * @param element the element to remove
	 * @return true on success, false on fail
	 */
	public boolean deleteElement(Element element) {
		for(Element e : elements)
		{
			if(e == element)
			{
				e.setParentCollection(null);
				elements.remove(e);
				
				return true;
			}
		}
		return false;
		// TODO implement this - Done
	}

	/**
	 * Returns the list of elements.
	 * 
	 * @return the list of elements
	 */
	public List<Element> getElements() {
		return elements;
		// TODO implement this - Done
	}

	/**
	 * Return the books that the given author wrote, by him/herself or as co-author,
	 * in this Collection. Return null if the given author didn't write anything. 
	 * If the author wrote several books, return all the books. 
	 *
	 * @param author the author that want to find
	 * @return Return the book titles that the given author wrote
	 */
	public Set<Book> findTitle(String author) {
		Set<Book> books = new HashSet<Book>();
		
		for(Element e : elements)
		{
			if(e instanceof Book)
			{
				for(String s : ( (Book)e ).getAuthor())
				{
					if(s == author)
					{
						books.add( (Book)e );
						break;
					}
				}
			}
			else
			{
				Set<Book> subBooks = ( (Collection)e ).findTitle(author);
				books.addAll(subBooks);
			}
		}
		
		return books;
		// TODO implement this - Done
	}
	
	public Set<Book> gatherBooks()
	{
		Set<Book> books = new HashSet<Book>();
		
		for(Element e : elements)
		{
			if(e instanceof Book)
			{
				books.add( (Book)e );
			}
			else
			{
				Set<Book> subBooks = ( (Collection) e).gatherBooks();
				books.addAll(subBooks);
			}
		}
		return books;
	}
	
	public Set<Book> findBooks(String collectionName)
	{
		Set<Book> books = new HashSet<Book>();
		
		for(Element e : elements)
		{
			if(e instanceof Collection)
			{
				if( (( (Collection)e ).getName()).equals(collectionName))
				{
					books.addAll( ((Collection)e).gatherBooks() );
					return books;
				}
				else
				{
					books.addAll( ( (Collection)e ).findBooks(collectionName) );
					
					if(books.size() != 0)
						return books;
				}
			}
			else
			{
				//Don't do anything
			}
		}
		return books;
	}
}






