package edu.postech.csed332.homework2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileWriter;
import java.io.BufferedReader;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.BufferedWriter;

/**
 * Container class for all the collections (that eventually contain books). The
 * library object is just a container for all collections. A library can be
 * exported to or imported from a file.
 * 
 * The format of the file is of your choice. Again, we strongly encourage using
 * some library to convert between Library objects and string representation.
 */
public final class Library {
	private List<Collection> collections;

	/**
	 * Builds a new, empty library.
	 */
	public Library() {
		collections = new ArrayList<Collection>();
		// TODO implement this - Done
	}

	/**
	 * Builds a new library and restores its contents from the given file.
	 *
	 * @param fileName the file from where to restore the library.
	 */
	public Library(String fileName) {
		collections = new ArrayList<Collection>();
		
		
		String str = "";
		try {
		File file = new File(fileName);
		FileReader filereader = new FileReader(file);
		BufferedReader bufReader = new BufferedReader(filereader);
		
		str = bufReader.readLine();
		bufReader.close();
		}
		catch (FileNotFoundException e) {}
		catch(IOException e) {}
		
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(str);
		
		Set<Map.Entry<String, JsonElement>> entries = element.getAsJsonObject().entrySet();
		for(Map.Entry<String, JsonElement> entry : entries)
		{
			Collection temp = Collection.restoreCollection(entry.getValue().getAsString());
			
			addCollection(temp);
		}
		// TODO implement this - Done
	}

	/**
	 * Saved the contents of the library to the given file.
	 *
	 * @param fileName the file where to save the library
	 */
	public void saveLibraryToFile(String fileName) {
		try {
			File file = new File(fileName);
			BufferedWriter bW = new BufferedWriter(new FileWriter(file));
			
			Gson gson = new Gson();
			
			JsonObject library = new JsonObject();
			
			int i = 1;
			for(Collection c : collections)
			{
				String temp = c.getStringRepresentation();
				library.addProperty(String.valueOf(i), temp);
				
				i++;
			}
			
			String json = gson.toJson(library);
			bW.write(json);
			
			bW.close();
		}
		catch(IOException e) {}
		
		// TODO implement this - Done
	}

	/**
	 * Returns the collections contained in the library.
	 *
	 * @return library contained elements
	 */
	public List<Collection> getCollections() {
		return collections;
		// TODO implement this - Done
	}
	
	public void addCollection(Collection c)
	{
		collections.add(c);
	}
	public boolean deleteCollection(String name)
	{
		List<Collection> cs = this.getCollections();
		for(Collection c : cs)
		{
			if(c.getName() == name)
			{
				collections.remove(c);
				return true;
			}
		}
		return false;
	}

	/**
	 * Return the set of all the books that belong to the given collection in the Library.
	 * Return null if the given collection doesn't exist. 
	 * Note that the name of the collection is unique and the findBooks should go through
	 * all the collections in the hierarchy of the given collection.
	 * Consider the following.
	 * (1) Computer Science is a collection. 
	 * (2) The "Introduction of Computer Science" is a book under Computer Science. 
	 * (3) Software Engineering is a collection under Computer Science. 
	 * (4) The "Software design method" is a book under Software Engineering collection. 
	 * (5) Development Methodology is a collection under Software Engineering.
	 * (6) The "Agile Programming" is a book under Development Methodology.
	 * 
	 * Then, findBooks method for the Computer Science should return a set of these 
	 * three Book objects "Introduction of Computer Science", "Software design method",
	 * and "Agile Programming".
	 * 
	 * @param collection the collection that want to know the belonging books
	 * @return Return the set of the books that belong to the given collection
	 */
	public Set<Book> findBooks(String collection) {
		Set<Book> result = new HashSet<Book>();
		result.clear();
		
		for(Collection c : collections)
		{
			if((c.getName()).equals(collection))
			{
				result.addAll(c.gatherBooks());
				return result;
			}
			else
			{
				result.addAll(c.findBooks(collection));
				if(result.size() != 0)
					return result;
			}
		}
		
		return null;
		
		// TODO implement this
	}

}