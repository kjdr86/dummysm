package edu.postech.csed332.homework2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * A book will contain only the title and the author(s).
 * 
 * A book can be exported to and imported from a string representation. 
 * The format of the string is of your choice, but by using the format, 
 * you should be able to construct a book object.
 * 
 * Do not reinvent the wheel. We strongly recommend that 
 * you use some library for XML, JSON, YML, or similar format 
 * instead of writing your own parsing code.
 * 
 * While using the library requires adding it to pom.xml, 
 * it will be overall faster for you, likely result in less buggy code, 
 * and you will get to learn more about Maven and Java.
 */
public final class Book extends Element {
    private String title;
    private Set<String> authors;

    /**
     * Builds a book with the given title and authors.
     *
     * @param title the title of the book
     * @param authors the authors of the book
     */
    public Book(String title, Set<String> authors) {
    	this.title = title;
    	this.authors = authors;
		// TODO implement this - Done
    }

    /**
     * Builds a book from the string representation, 
     * which contains the title and authors of the book. 
     *
     * @param stringRepresentation the string representation
     */
    public Book(String stringRepresentation) {
    	this.authors = new HashSet<String>();
    	authors.clear();
    	
    	JsonParser parser = new JsonParser();
    	JsonElement element = parser.parse(stringRepresentation);
    	
    	this.title = element.getAsJsonObject().get("title").getAsString();
    	JsonObject arr = element.getAsJsonObject().get("authors").getAsJsonObject();
    	
    	Set<Map.Entry<String, JsonElement>> entries = arr.entrySet();
    	for(Map.Entry<String, JsonElement> entry : entries)
    	{
    		authors.add(entry.getValue().getAsString());
    	}
		// TODO implement this - Done maybe..?
    }

    /**
     * Returns the string representation of the given book.
     * The representation contains the title and authors of the book.
     *
     * @return the string representation
     */
    public String getStringRepresentation() {
    	Gson gson = new Gson();
    	
    	JsonObject book = new JsonObject();
    	book.addProperty("title", this.title);
    	
    	JsonObject authorsAsString = new JsonObject();
    	
    	int i = 1;
    	for(String author : authors)
    	{
    		authorsAsString.addProperty(String.valueOf(i), author);
    		i++;
    	}
    	
    	JsonElement element = gson.fromJson(authorsAsString.toString(), JsonElement.class);
    	book.add("authors", element);
    	
    	
    	String json = gson.toJson(book);
    	
    	return json;
		// TODO implement this - Done maybe..??
    }

    /**
     * Returns all the collections that this book belongs to directly and indirectly.
     * Consider the following. 
     * (1) Computer Science is a collection. 
     * (2) Operating Systems is a collection under Computer Science. 
     * (3) The Linux Kernel is a book under Operating System collection. 
     * Then, getContainingCollections method for The Linux Kernel should return a list 
     * of these two collections (Computer Science, Operating System), starting from 
     * the top-level collection.
     *
     * @return the list of collections
     */
    public List<Collection> getContainingCollections() {
    	List<Collection> parents = new ArrayList<Collection>();
    	
    	Element temp = this;
    	while(temp.getParentCollection() != null)
    	{
    		parents.add(temp.getParentCollection());
    		temp = temp.getParentCollection();
    	}
    	
    	Collections.reverse(parents);
    	
    	return parents;
    	
		// TODO implement this - Done
    }

    /**
     * Returns the title of the book.
     *
     * @return the title
     */
    public String getTitle() {
    	return this.title;
		// TODO implement this - Done
    }

    /**
     * Returns the authors of the book.
     *
     * @return the authors
     */
    public Set<String> getAuthor() {
    	return this.authors;
		// TODO implement this - Done
    }
    
}