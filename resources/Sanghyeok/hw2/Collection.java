package edu.postech.csed332.homework2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.*;

/**
 * The Collection class represents a single collection. It contains a name of
 * the collection and also has a list of references to every book and every
 * sub-collection in that particular collection.
 * 
 * A collection can be exported to and imported from a string representation.
 * The format of the string is of your choice, but the string representation
 * should have the name of this collection and all elements (books and
 * sub-collections) contained in this collection.
 */
public final class Collection extends Element {
	List<Element> elements;
	private String name;

	/**
	 * Creates a new collection with the given name.
	 *
	 * @param name the name of the collection
	 */
	public Collection(String name) {
		// TODO implement this
		elements = new ArrayList<Element>();
		this.name = name;
	}

	/**
	 * Restores a collection from its given string representation.
	 *
	 * @param stringRepresentation the string representation
	 */
	public static Collection restoreCollection(String stringRepresentation) {
		// TODO implement this
		JsonParser parser = new JsonParser();
		JsonElement elem = parser.parse(stringRepresentation);
		
		String n = elem.getAsJsonObject().get("name").getAsString();

		Collection c = new Collection(n);
		List<Element> elemList = new ArrayList<Element>();

		int bookCount = elem.getAsJsonObject().get("bookCount").getAsInt();
		for (int i = 0; i < bookCount; i++) {
			String s = elem.getAsJsonObject().get("book"+Integer.toString(i)).getAsString();
			s=s.replace("\\\"","");
			Book b=new Book(s);
			elemList.add(b);
		}
		int collectionCount = elem.getAsJsonObject().get("collectionCount").getAsInt();
		for (int i = 0; i < collectionCount; i++) {
			String s = elem.getAsJsonObject().get("collection"+Integer.toString(i)).getAsString();
			s=s.replace("\\\"","");
			Collection sub=Collection.restoreCollection(s);
			elemList.add(sub);
		}
		c.elements=elemList;
		return c;
	}

	/**
	 * Returns the string representation of a collection. The string representation
	 * should have the name of this collection, and all elements
	 * (books/subcollections) contained in this collection.
	 *
	 * @return string representation of this collection
	 */
	//book0,book1,collection0,collection1,bookCount,collectionCount
	public String getStringRepresentation() {
		// TODO implement this
		JsonObject data = new JsonObject();

		data.addProperty("name", name);
		int book_count = 0;
		int collection_count = 0;
		for (Element elem : elements) {
			if (elem instanceof Book) {
				String finaldata=(((Book) elem).getStringRepresentation());
				data.addProperty("book" + Integer.toString(book_count), finaldata);
				book_count++;
			}
			if (elem instanceof Collection) {
				String finaldata=(((Collection) elem).getStringRepresentation());
				
				data.addProperty("collection" + Integer.toString(collection_count),finaldata);
				collection_count++;
			}
		}
		data.addProperty("bookCount", book_count);
		data.addProperty("collectionCount", collection_count);
		
		return data.toString();
		
	}

	/**
	 * Returns the name of the collection.
	 *
	 * @return the name
	 */
	public String getName() {
		// TODO implement this
		return name;
	}

	/**
	 * Adds an element to the collection. If parentCollection of given element is
	 * not null, do not actually add, but just return false. (explanation: if given
	 * element is already a part of another collection, you should have deleted the
	 * element from that collection before adding to another collection)
	 *
	 * @param element the element to add
	 * @return true on success, false on fail
	 */
	public boolean addElement(Element element) {
		// TODO implement this
		if (element.getParentCollection() == null) {
			elements.add(element);
			element.setParentCollection(this);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Deletes an element from the collection. Returns false if the collection does
	 * not have this element. Hint: Do not forget to clear parentCollection of given
	 * element
	 *
	 * @param element the element to remove
	 * @return true on success, false on fail
	 */
	public boolean deleteElement(Element element) {
		// TODO implement this
		if (elements.contains(element)) {
			elements.remove(element);
			element.setParentCollection(null);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the list of elements.
	 * 
	 * @return the list of elements
	 */
	public List<Element> getElements() {
		// TODO implement this
		return elements;
	}

	/**
	 * Return the books that the given author wrote, by him/herself or as co-author,
	 * in this Collection. Return null if the given author didn't write anything. If
	 * the author wrote several books, return all the books.
	 *
	 * @param author the author that want to find
	 * @return Return the book titles that the given author wrote
	 */
	public Set<Book> findTitle(String author) {
		// TODO implement this
		boolean is_write = false;
		Set<Book> bookList = new HashSet<Book>();
		for (Element elem : elements) {
			if (elem instanceof Book) {
				Set<String> authorsList = ((Book) elem).getAuthor();
				if (authorsList.contains(author)) {
					bookList.add((Book) elem);
					is_write = true;
				}
			}
		}
		if (is_write)
			return bookList;
		else
			return null;
	}

}