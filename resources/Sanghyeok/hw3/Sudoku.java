package edu.postech.csed332.homework3;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * 
 * An instance of this class represents a even/odd Sudoku puzzle. This class
 * must be immutable. That is, you should define a constructor to create a
 * Sudoku object with all the necessary information for a concrete even/odd
 * Sudoku puzzle.
 * 
 * You can freely add member variables and methods to implement this class.
 * 
 */
public class Sudoku {

	int[][] table;
	Set<Solution> setSolution;

	// 0 means even(*), -1 means odd(.)
	// TODO implement this
	Sudoku(String filename) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(filename));
		table = new int[9][9];

		int i = 0;
		while (true) {
			String line = br.readLine();
			if (line == null)
				break;
			if (line.length() == 9) {
				for (int j = 0; j < 9; j++) {
					char c = line.charAt(j);
					if (c == '*') {
						table[i][j] = 0;
					} else if (c == '.') {
						table[i][j] = -1;
					} else {
						table[i][j] = c - '0';
					}
				}
			} else {
				System.out.println("Illegal Format");
				break;
			}
			i++;
		}
		br.close();
	}

	/**
	 * Returns a set of Solution instance for the given sudoku puzzle.
	 * 
	 * @return the set of solutions
	 */
	// enocded as i*81+j*9+k
	public Set<Solution> solve() {
		// TODO implement this
		setSolution = new HashSet<Solution>();
		List<int[]> clauseList = new ArrayList<int[]>();
		Map<Integer, List<Integer>> row_evenMap = new HashMap<Integer, List<Integer>>(); //key: row number / value: column position of even number in the row
		Map<Integer, List<Integer>> row_oddMap = new HashMap<Integer, List<Integer>>();
		Map<Integer, List<Integer>> col_evenMap = new HashMap<Integer, List<Integer>>();
		Map<Integer, List<Integer>> col_oddMap = new HashMap<Integer, List<Integer>>();
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (table[i][j] == -1) {
					int[] clause = new int[] { i * 81 + j * 9 + 1, i * 81 + j * 9 + 3, i * 81 + j * 9 + 5,
							i * 81 + j * 9 + 7, i * 81 + j * 9 + 9 };
					clauseList.add(clause);
					if (row_oddMap.containsKey(i)) {
						List<Integer> temp = row_oddMap.get(i);
						temp.add(j);
						row_oddMap.put(i, temp);
					} else {
						List<Integer> temp = new ArrayList<Integer>();
						temp.add(j);
						row_oddMap.put(i, temp);
					}
					if (col_oddMap.containsKey(j)) {
						List<Integer> temp = col_oddMap.get(j);
						temp.add(i);
						col_oddMap.put(j, temp);
					} else {
						List<Integer> temp = new ArrayList<Integer>();
						temp.add(i);
						col_oddMap.put(j, temp);
					}
				} else if (table[i][j] == 0) {
					int[] clause = new int[] { i * 81 + j * 9 + 2, i * 81 + j * 9 + 4, i * 81 + j * 9 + 6,
							i * 81 + j * 9 + 8 };
					clauseList.add(clause);
					if (row_evenMap.containsKey(i)) {
						List<Integer> temp = row_evenMap.get(i);
						temp.add(j);
						row_evenMap.put(i, temp);
					} else {
						List<Integer> temp = new ArrayList<Integer>();
						temp.add(j);
						row_evenMap.put(i, temp);
					}
					if (col_evenMap.containsKey(j)) {
						List<Integer> temp = col_evenMap.get(j);
						temp.add(i);
						col_evenMap.put(j, temp);
					} else {
						List<Integer> temp = new ArrayList<Integer>();
						temp.add(i);
						col_evenMap.put(j, temp);
					}
				} else {
					clauseList.add(new int[] { i * 81 + j * 9 + (table[i][j]) });
				}
			}
		}
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (table[i][j] != -1 && table[i][j] != 0) {
					if (isEven(i + 1, j + 1)) {
						List<Integer> rowList = row_evenMap.get(i);
						List<Integer> colList = col_evenMap.get(j);
						if (rowList != null) {
							for (int col : rowList) {
								int[] tempRow = new int[] { (-1) * (i * 81 + col * 9 + table[i][j]) };
								clauseList.add(tempRow);
							}
						}
						if (colList != null) {
							for (int row : colList) {
								int[] tempCol = new int[] { ((-1) * (row * 81 + j * 9 + table[i][j])) };
								clauseList.add(tempCol);
							}
						}

						for (int k = 0; k < 3; k++) {
							List<Integer> squareRow = row_evenMap.get(k + 3 * (i / 3));
							if (squareRow != null) {
								for (int col : squareRow) {
									if (col >= 3 * (j / 3) && col < (3 + 3 * (j / 3))) {
										int[] tempRow = new int[] {
												(-1) * (((k + 3 * (i / 3)) * 81 + col * 9 + table[i][j])) };
										clauseList.add(tempRow);
									}
								}
							}
						}

					} else {
						List<Integer> rowList = row_oddMap.get(i);
						List<Integer> colList = col_oddMap.get(j);

						if (rowList != null) {
							for (int col : rowList) {
								int[] tempRow = new int[] { (-1) * (i * 81 + col * 9 + table[i][j]) };
								clauseList.add(tempRow);
							}
						}
						if (colList != null) {
							for (int row : colList) {
								int[] tempCol = new int[] { ((-1) * (row * 81 + j * 9 + table[i][j])) };
								clauseList.add(tempCol);
							}
						}

						for (int k = 0; k < 3; k++) {
							List<Integer> squareRow = row_oddMap.get(k + 3 * (i / 3));
							if (squareRow != null) {
								for (int col : squareRow) {
									if (col >= 3 * (j / 3) && col < (3 + 3 * (j / 3))) {
										int[] tempRow = new int[] {
												(-1) * (((k + 3 * (i / 3)) * 81 + col * 9 + table[i][j])) };
										clauseList.add(tempRow);
									}
								}
							}
						}
					}
				} else if (table[i][j] == -1) {
					List<Integer> rowList = row_oddMap.get(i);
					List<Integer> colList = col_oddMap.get(j);
					if (rowList != null) {
						for (int col : rowList) {
							if (j != col) {
								for (int k = 1; k <= 5; k++) {
									int[] tempRow = new int[] { (-1) * (i * 81 + col * 9 + 2 * k - 1),
											(-1) * (i * 81 + j * 9 + 2 * k - 1) };
									clauseList.add(tempRow);
								}
							}
						}
					}
					if (colList != null) {
						for (int row : colList) {
							if (i != row) {
								for (int k = 1; k <= 5; k++) {
									int[] tempRow = new int[] { (-1) * (row * 81 + j * 9 + 2 * k - 1),
											(-1) * (i * 81 + j * 9 + 2 * k - 1) };
									clauseList.add(tempRow);
								}
							}
						}
					}
					for (int k = 0; k < 3; k++) {
						List<Integer> squareRow = row_oddMap.get(k + 3 * (i / 3));
						if (squareRow != null) {
							for (int col : squareRow) {
								if (!(i == (k + 3 * (i / 3)) && j == col) && col >= 3 * (j / 3)
										&& col < (3 + 3 * (j / 3))) {
									for (int w = 1; w <= 5; w++) {
										int[] tempRow = new int[] { (-1) * (i * 81 + j * 9 + 2 * w - 1),
												(-1) * (81 * (k + 3 * (i / 3)) + 9 * col + 2 * w - 1) };
										clauseList.add(tempRow);
									}
								}
							}
						}
					}

				} else if (table[i][j] == 0) {
					List<Integer> rowList = row_evenMap.get(i);
					List<Integer> colList = col_evenMap.get(j);
					if (rowList != null) {
						for (int col : rowList) {
							if (col != j) {
								for (int k = 1; k <= 4; k++) {
									int[] tempRow = new int[] { (-1) * (i * 81 + col * 9 + 2 * k),
											(-1) * (i * 81 + j * 9 + 2 * k) };
									clauseList.add(tempRow);
								}
							}
						}
					}
					if (colList != null) {
						for (int row : colList) {
							if (row != i) {
								for (int k = 1; k <= 4; k++) {
									int[] tempCol = new int[] { (-1) * (row * 81 + j * 9 + 2 * k),
											(-1) * (i * 81 + j * 9 + 2 * k) };
									clauseList.add(tempCol);
								}
							}
						}
					}
					for (int k = 0; k < 3; k++) {
						List<Integer> squareRow = row_evenMap.get(k + 3 * (i / 3));
						if (squareRow != null) {
							for (int col : squareRow) {
								if (!(i == (k + 3 * (i / 3)) && j == col) && col >= 3 * (j / 3)
										&& col < (3 + 3 * (j / 3))) {
									for (int w = 1; w <= 4; w++) {
										int[] tempRow = new int[] { (-1) * (i * 81 + j * 9 + 2 * w),
												(-1) * (81 * (k + 3 * (i / 3)) + 9 * col + 2 * w) };
										clauseList.add(tempRow);
									}
								}
							}
						}
					}
				}
			}
		}
		int[][] final_clause = new int[clauseList.size()][];
		for (int i = 0; i < clauseList.size(); i++) {
			int[] temp = clauseList.get(i);
			final_clause[i] = temp;
		}
		try {
			while (true) {
				Solution s = new Solution();
				s.solve(final_clause);
				if (s.get_oneSolution()) {
					setSolution.add(s);
					break;
				} else {
					setSolution.add(s);
					int[][] modified_clause = new int[final_clause.length + 1][];
					for (int i = 0; i < final_clause.length; i++) {
						modified_clause[i] = final_clause[i];
					}
					int[] must_add_clause=new int[81];
					for(int i=0;i<9;i++) {
						for(int j=0;j<9;j++) {
							must_add_clause[i*9+j]=(-1)*(i*81+j*9+s.getValue(i+1, j+1));
						}
					}
					modified_clause[final_clause.length]=must_add_clause;
					final_clause = modified_clause;
				}
			}
		} catch (Exception E) {
			System.err.println("error");
		}
		return setSolution;
	}

	/**
	 * Returns true if a given cell must contain an even number.
	 * 
	 * @param row    row number
	 * @param column column number
	 * @return true if a cell (row, column) must be filled by an even number;
	 *         otherwise, return false.
	 */
	public Boolean isEven(int row, int column) {
		// TODO implement this
		if (table[row - 1][column - 1] == 0 || table[row - 1][column - 1] % 2 == 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Return the initial value of a cell, given an even/odd Sudoku puzzle. If there
	 * is no value to get (e.g. out of range, or empty), return null.
	 * 
	 * @param row    row number
	 * @param column column number
	 * @return A value of a cell, if it is non-empty
	 */
	public Integer getValue(int row, int column) {
		// TODO implement this
		int target = table[row - 1][column - 1];
		if (target == 0 || target == -1) {
			return null;
		} else {
			return target;
		}
	}

}