package edu.postech.csed332.homework3;

import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;
import org.sat4j.tools.*;

/**
 * 
 * An instance of this class represents a single solution for a sudoku puzzle
 * 
 * You can freely add member variables and methods to implement this class
 * 
 */
public class Solution {

	ISolver solver;
	private int numVar;
	boolean [] result;
	boolean oneSolution;
	
	Solution() {
		this.numVar = 729;
		this.solver = SolverFactory.newDefault();
		this.solver.newVar(729);
	}
	
	void solve(int[][] clauses) throws ContradictionException, TimeoutException {
		this.solver.setExpectedNumberOfClauses(clauses.length);
		for (int i = 0; i < clauses.length; ++i) {
			this.solver.addClause(new VecInt(clauses[i]));
		}

		if (this.solver.isSatisfiable()) {
			result = new boolean[numVar];
			for(int i=0; i < numVar; i++) {
				result[i]=solver.model(i+1);
			}
			SingleSolutionDetector detector= new SingleSolutionDetector(solver);
			oneSolution=detector.hasASingleSolution();
		}
  	}
	//TODO implement this
	/**
	 * Returns a value of a cell in the solution. If the input is out of range, return null.
	 * 
	 * For example, in the example puzzle in the homework description, cell (1,3) will be 
	 * filled by number 8 by the solution. In this case, getValue(1,3) should return 8.
	 * 
	 * @param row row number
	 * @param column column number
	 * @return Value of a (row, column) cell, provided by a given solution
	 */
	boolean get_oneSolution() {
		return oneSolution;
	}
	public Integer getValue(int row, int column) {
		//TODO implement this
		int position=81*(row-1)+9*(column-1);
		int value=0;
		for(int i=0;i<9;i++) {
			if(result[position+i]) {
				value=i+1;
			}
		}
		return value;
	}
}
