package edu.postech.csed332.homework3;

import java.io.PrintWriter;
import java.io.IOException;
import java.util.Set;

/**
 * Receives a sudoku puzzle as a file by the command line argument, solves the
 * puzzle, and makes a file for each solution
 * 
 * For example, if you received an input sudoku file "A" and the solver gives 3
 * solutions for the puzzle, the output files "A_1.solution", "A_2.solution" and
 * "A_3.solution" will be made.
 *
 */
public class Game {
	
	public static int number_of_solution=0;
	// TODO implement this

	/**
	 * @param args name of the input file for a sudoku puzzle
	 */
	static void solutionWrite(Solution s, String filename) throws IOException {
		PrintWriter pw = new PrintWriter(filename);
		for (int i = 1; i <= 9; i++) {
			String line = "";
			for (int j = 1; j <= 9; j++) {
				line = line + s.getValue(i, j).toString();
			}
			pw.println(line);
		}
		pw.close();
	}

	public static void main(String[] args) {
		// TODO implement this
		try {
			Sudoku sudokuTable = new Sudoku(args[0]);
			Set<Solution> solutionSet = sudokuTable.solve();
			int i = 1;
			for (Solution s : solutionSet) {
				solutionWrite(s, args[0]+"_"+ i + ".solution");
				i++;
			}
			number_of_solution=i-1;
		} catch (Exception E) {
			System.err.println("error");
		}
	}

}
