package edu.postech.csed332.homework1;

/**
 * The interest of HighInterest account is 1% per day. For example, one person
 * puts 100 dollars in his HighInterest Account(At Day 1). After 10 days(Day
 * 11), the balance of the account will be 100*(1.01)^10. The balance of
 * HighInterest account should always bigger or equal to 1000.
 */
class HighInterest implements Account {

	final double INTEREST = 1.01;
	private int AccountNumber;
	private double Balance;
	private String Owner;

	HighInterest(String name, double initial, int num) {
		Owner = name;
		Balance = initial;
		AccountNumber = num;
	}

	public int getAccountNumber() {
		return AccountNumber;
	}

	public double getBalance() {
		return Balance;
	}

	public String getOwner() {
		return Owner;
	}

	public void updateBalance(int elapsedDate) {
		double elapse = (double) elapsedDate;
		double mult = (Math.pow(INTEREST, elapse));
		Balance = Balance * mult;
	}

	public void deposit(double amount) {
		Balance += amount;
	}

	public void withdraw(double amount) throws NotEnoughException {
		if (amount > Balance) {
			throw new NotEnoughException();
		} else {
			if ((Balance - amount) >= 1000) {
				Balance-=amount;
			}else {
				System.out.println("High Interest Account should have balance bigger or equal to 1000.");
				throw new NotEnoughException();
			}
		}
	}

}