package edu.postech.csed332.homework1;

/**
 * The interest of LowInterest account is 0.5% per day. For example, one person puts 100
 * dollars in his LowInterest Account(At Day 1). After 10 days(Day 11), 
 * the balance of the account will be 100*(1.005)^10.
 */
class LowInterest implements Account {
	
	final double INTEREST=1.005;
	private int AccountNumber;
	private double Balance;
	private String Owner;
	

	LowInterest (String name, double initial,int num){
		Owner=name;
		Balance=initial;
		AccountNumber=num;
	}
	public int getAccountNumber() {
		return AccountNumber;
	}
	public double getBalance() {
		return Balance;
	}
	public String getOwner() {
		return Owner;
	}
	public void updateBalance(int elapsedDate) {
		double elapse=(double)elapsedDate;
		double mult=(Math.pow(INTEREST,elapse));
		Balance= Balance*mult;
	}
	public void deposit(double amount) {
		Balance+=amount;
	}
	public void withdraw(double amount) throws NotEnoughException{
		if(amount>Balance) {
			throw new NotEnoughException();
		}
		else {
			Balance-=amount;
		}
	}
}