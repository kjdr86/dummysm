package unitTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.jdt.core.IAnnotation;
import org.eclipse.jdt.core.IClassFile;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.ILocalVariable;
import org.eclipse.jdt.core.IMemberValuePair;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IOpenable;
import org.eclipse.jdt.core.ISourceRange;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.ITypeParameter;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import metric.CyclomaticComplexity;

public class CyclomaticComplexityTest {
	public class MockIMethod implements IMethod {
		String fileName;
		public MockIMethod(int _fileNum) {
			this.fileName = "method"+Integer.toString(_fileNum)+".txt";
		}

		
		@Override
		public String[] getCategories() throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IClassFile getClassFile() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ICompilationUnit getCompilationUnit() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IType getDeclaringType() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getFlags() throws JavaModelException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public ISourceRange getJavadocRange() throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getOccurrenceCount() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public IType getType(String arg0, int arg1) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ITypeRoot getTypeRoot() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean isBinary() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean exists() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public IJavaElement getAncestor(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getAttachedJavadoc(IProgressMonitor arg0) throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IResource getCorrespondingResource() throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getElementType() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public String getHandleIdentifier() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IJavaModel getJavaModel() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IJavaProject getJavaProject() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IOpenable getOpenable() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IJavaElement getParent() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IPath getPath() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IJavaElement getPrimaryElement() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IResource getResource() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ISchedulingRule getSchedulingRule() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IResource getUnderlyingResource() throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean isReadOnly() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isStructureKnown() throws JavaModelException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public <T> T getAdapter(Class<T> adapter) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ISourceRange getNameRange() throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getSource() throws JavaModelException {
			Path path = Paths.get(System.getProperty("user.dir"), "resources","CyclomaticComplexityResources", this.fileName);
			byte[] encoded = null;
			try {
				encoded = Files.readAllBytes(path);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return new String(encoded, StandardCharsets.UTF_8);
		}

		@Override
		public ISourceRange getSourceRange() throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void copy(IJavaElement arg0, IJavaElement arg1, String arg2, boolean arg3, IProgressMonitor arg4)
				throws JavaModelException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void delete(boolean arg0, IProgressMonitor arg1) throws JavaModelException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void move(IJavaElement arg0, IJavaElement arg1, String arg2, boolean arg3, IProgressMonitor arg4)
				throws JavaModelException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void rename(String arg0, boolean arg1, IProgressMonitor arg2) throws JavaModelException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public IJavaElement[] getChildren() throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean hasChildren() throws JavaModelException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public IAnnotation getAnnotation(String arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IAnnotation[] getAnnotations() throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IMemberValuePair getDefaultValue() throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getElementName() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String[] getExceptionTypes() throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getKey() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getNumberOfParameters() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public String[] getParameterNames() throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String[] getParameterTypes() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ILocalVariable[] getParameters() throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String[] getRawParameterNames() throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getReturnType() throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getSignature() throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ITypeParameter getTypeParameter(String arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String[] getTypeParameterSignatures() throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ITypeParameter[] getTypeParameters() throws JavaModelException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean isConstructor() throws JavaModelException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isLambdaMethod() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isMainMethod() throws JavaModelException {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isResolved() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isSimilar(IMethod arg0) {
			// TODO Auto-generated method stub
			return false;
		}

	}
	
	MockIMethod method;

	@ParameterizedTest(name = "method{0}.txt should be {1}")
	@CsvSource({"0, 8", "1, 2", "2, 52", "3, 70"})
	void calcCyclomaticComplexityTest(int fileNum, int result) {
		method = new MockIMethod(fileNum);
		assertEquals(result, CyclomaticComplexity.calcCyclomaticComplexity(method));
	}
}
