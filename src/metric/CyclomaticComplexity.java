package metric;

import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.JavaModelException;

public class CyclomaticComplexity {
	public static int calcCyclomaticComplexity(IMethod method) {
		String source = null;
		try {
		source = method.getSource();
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
		int num_if = 0;
		int num_elseif = 0;
		int num_else = 0;
		int num_for = 0;
		int num_while = 0;
		
		//Split the source string into white spaces
		String[] words = source.split("\\W+");
		
		for(int i= 0 ; i < words.length ; i++) {
			String word = words[i];
			if(word.equals("if")) {
				num_if++;
			}
			else if (word.equals("else")) {
				if(i + 1 < words.length && words[i + 1].equals("if")) {
					num_elseif++;
					i++;
					continue;
				}
				else {
					num_else++;
				}
			}
			else if (word.equals("for")) {
				num_for++;
			}
			else if (word.equals("while")) {
				num_while++;
			}
		}
		//For debug purpose
		//System.out.format("%d %d %d %d %d\n", num_if, num_elseif, num_else, num_for, num_while);
		return num_if + num_elseif + num_else + num_for + num_while + 1;
	}
}
